# 谷粒商城

#### 介绍

顾沉舟，一个风度偏偏的少年

#### 软件架构
采用微服务架构


#### 大体技术一览

1.  配置中心/注册中心 SpringcloudAlibaba nacos
2.  服务熔断/服务降级 SpringcloudAlibaba sentinel
3.  网关 Springcloud gateway
4.  远程调用 Springcloud fegin
5.  Springboot2.x(包括Spring5中的核心Webflux编程,在这个项目中有具体应用),关于Webflux可以参考我的掘金博客:
6.  mybatis/mybatis-plus
7.  vue组件化开发
8.  阿里云oss对象存储


#### 配置环境
##### 操作系统： mac/linux
##### 应用容器引擎：docker
##### 数据库：mysql，Redis
##### 脚手架：逆向工程&人人开源
##### 测试工具: postman


#### 使用说明

1.  搭建好所需环境
2.  修改数据库url,账号，密码

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
