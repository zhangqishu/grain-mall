package com.zqs.grainmall.web;

import com.zqs.grainmall.product.entity.CategoryEntity;
import com.zqs.grainmall.product.service.CategoryService;
import com.zqs.grainmall.product.vo.Catelog2Vo;
import org.redisson.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * @program: grain-mall
 * @description: 页面跳转
 * @author: Mr.Zhang
 * @create: 2020-11-04 11:25
 **/
@Controller
public class IndexController {

    @Autowired
    CategoryService categoryService;

    @Autowired
    RedissonClient redissonClient;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @GetMapping({"/","/index"})
    public String indexPage(Model model){

        //TODO 1.查出所有的1级分类
       List<CategoryEntity> categoryEntities=categoryService.getLevel1Categorys();


        //视图解析器进行拼串:
        //前缀: classpath: /templates/ +返回值 +.html
        //后缀: .html

        model.addAttribute("categorys",categoryEntities);
        return "index";

    }

    //index/catalog.json
    @ResponseBody
    @GetMapping("/index/catalog.json")
    public  Map<String, List<Catelog2Vo>> getCatalogJson(){
        Map<String, List<Catelog2Vo>> catelogJson = categoryService.getCatelogJson();
        return catelogJson;
    }

    @ResponseBody
    @GetMapping("/hello")
    public String hello(){

        //1.获取一把锁，只要锁名一样，就是同一把锁
        RLock lock = redissonClient.getLock("my-lock");

        //2.加锁
        lock.lock();  //阻塞式等待,默认加的锁都是30秒时间
        //TODO redisson解决了2个问题，
        // 1.锁的自动续期，如果业务超长，运行期间自动给锁续上新的30s,不用担心业务时间长，锁自动过期被删掉 --》看门狗机制
        // 2.加锁的业务只要运行完成，就不会给当前锁续期，即使不手动解锁，锁默认在30秒以后自动删除

        // 10秒自动解锁，自动解锁时间一定要大于业务的执行时间
        //lock.lock(10, TimeUnit.SECONDS); //这段代码有一个问题就是:在锁时间到了以后不会自动续期
        //1.上述代码如果我们传递了锁的超时时间，就发送给redis执行脚本，进行占锁，默认超时就是我们指定的时间
        //2.如果我们未指定锁的超时时间，就使用30*1000 【LockWatchdogTimeout看门狗的默认时间】
        //   只要占锁成功，就会启动一个定时任务[重新给锁设置过期时间,新的过期时间就是看门狗的默认时间]，每隔10s都会自动再次续期，续成30s
        //   定时任务多长时间执行一次？ internalLockLeaseTime[看门狗时间]/3 10s

        //最佳实战
        //1.lock.lock(10, TimeUnit.SECONDS); 省掉了整个续期操作。手动解锁
        try {
            System.out.println("加锁成功,执行业务...."+Thread.currentThread().getId());
            Thread.sleep(30000);
        }catch (Exception e)
        {

        }finally {
            //3.解锁 ，假设解锁代码没有运行，redssion会不会出现死锁
            System.out.println("释放锁...."+Thread.currentThread().getId());
            lock.unlock();
        }

        return "hello";
    }

    /**
     * 保证一定能读到最新数据，修改期间，写锁是一个排他锁(互斥锁，独享锁)。读锁是一个共享锁
     * 写锁没释放读就必须等待
     * 读+读 : 相当于无锁，并发读，只会在redis中记录好，所有当前读读锁他们都会同时加锁成功
     * 写+读 : 等待写锁释放再读
     * 写+写 : 阻塞方式
     * 读+写 : 先等待读锁释放再写
     *
     * 总结:只要有写的存在，都必须等待
     *
     * */
    @ResponseBody
    @GetMapping("/write")
    public String wirteValue(){

        RReadWriteLock lock = redissonClient.getReadWriteLock("rw-lock");
        String s="";
        //1.改数据加写锁
        RLock rLock = lock.writeLock();
        try {
            rLock.lock();
            System.out.println("写锁加锁成功"+Thread.currentThread().getId());
            s = UUID.randomUUID().toString();
            Thread.sleep(30000);
            stringRedisTemplate.opsForValue().set("writeValue",s);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            //解锁
            rLock.unlock();
            System.out.println("写锁解锁成功"+Thread.currentThread().getId());
        }
        return s;
    }

    @ResponseBody
    @GetMapping("/read")
    public String readValue(){

        RReadWriteLock lock = redissonClient.getReadWriteLock("rw-lock");
        String s="";
        //2.读数据 加 读锁
        RLock rLock = lock.readLock();
        try {

            rLock.lock();
            System.out.println("读锁加锁成功"+Thread.currentThread().getId());
            Thread.sleep(30000);

           s= stringRedisTemplate.opsForValue().get("writeValue");
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            //解锁
            rLock.unlock();
            System.out.println("读锁解锁成功"+Thread.currentThread().getId());
        }
        return s;
    }

    /**
     * 闭锁:
     *  例子:放假，锁门
     *  1班2班3班4班5班没人了
     *  5个班全部走完，我们就可以锁大门了
     *
     *  也就是我下面这个服务
     *  只有当我的/gogogo{id}这个请求满5次，我的闭锁/lockDoor请求才会执行完成
     *
     *
     *  door.trySetCount(5); //设置计数为5次
     *
     *  door.countDown();  //计数减一
     *
     * */
    @GetMapping("/lockDoor")
    @ResponseBody
    public String lockDoor() throws InterruptedException {
        //闭锁
        RCountDownLatch door = redissonClient.getCountDownLatch("door");
        door.trySetCount(5);//设置计数为5次
        door.await(); //等待闭锁都完成

        return "放假了.....";
    }

    @GetMapping("/gogogo/{id}")
    @ResponseBody
    public String gogogo(@PathVariable("id") Long id){
        RCountDownLatch door = redissonClient.getCountDownLatch("door");
        door.countDown();  //计数减一
        return id+"班的人都走了";
    }


    /**
     * 信号量:
     *   什么意思呢？
     *   车路停车，3个车位，想要停车得看车位够不够
     *
     *  它也可以做分布式限流
     * */

    @GetMapping("/park")
    @ResponseBody
    public String park() throws InterruptedException {

        RSemaphore park = redissonClient.getSemaphore("park"); //park的值设为了3  假设设为x
      //  park.acquire(); //获取一个信号，获取一个值,占一个车位  x-1 当x=0时则一直等待(阻塞式获取，就是我一定要获取成功)
        boolean b = park.tryAcquire();  //有就占，没有就算了  有就返回true，没有就返回false,不会进行阻塞
        if (b)
        {
            //执行业务
        }else
        {
            return "error";
        }

        return "ok=》"+b;
    }

    @GetMapping("/go")
    @ResponseBody
    public String go() throws InterruptedException {

        RSemaphore park = redissonClient.getSemaphore("park");
        park.release();      //释放一个车位  x+1

        return "ok";
    }





}
