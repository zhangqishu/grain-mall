package com.zqs.grainmall.product.dao;

import com.zqs.grainmall.product.entity.SkuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * sku信息
 * 
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 15:54:09
 */
@Mapper
public interface SkuInfoDao extends BaseMapper<SkuInfoEntity> {
	
}
