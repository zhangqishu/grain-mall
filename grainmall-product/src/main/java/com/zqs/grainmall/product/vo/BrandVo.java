package com.zqs.grainmall.product.vo;

import lombok.Data;

/**
 * @program: grain-mall
 * @description: 品牌有用的信息
 * @author: Mr.Zhang
 * @create: 2020-10-27 11:37
 **/
@Data
public class BrandVo {

    private Long brandId;
    private String brandName;
}
