package com.zqs.grainmall.product.vo;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-16 13:52
 **/
@ToString
@Data
public class SkuItemSaleAttrVo {
    /**
     * 属性id
     */
    private Long attrId;
    /**
     * 属性名
     */
    private String attrName;
    private List<AttrValueWithSkuIdVo> attrValues;
}
