package com.zqs.grainmall.product.dao;

import com.zqs.grainmall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 15:54:10
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
