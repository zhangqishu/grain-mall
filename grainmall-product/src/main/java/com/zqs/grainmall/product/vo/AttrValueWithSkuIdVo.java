package com.zqs.grainmall.product.vo;

import lombok.Data;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-16 14:54
 **/
@Data
public class AttrValueWithSkuIdVo {
    private String attrValue;
    private String skuIds;
}
