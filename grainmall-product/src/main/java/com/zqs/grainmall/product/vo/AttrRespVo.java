package com.zqs.grainmall.product.vo;

import lombok.Data;

/**
 * @program: grain-mall
 * @description: 响应
 * @author: Mr.Zhang
 * @create: 2020-10-26 09:47
 **/
@Data
public class AttrRespVo  extends AttrVo{

    /**
     *  cateLogName  "手机/数码/手机" // 所属分类名字
     *  groupName    " 主体"    //所属分组的名字
     *
     * */
    private String cateLogName;

    private String groupName;

    private Long[] catelogPath;


}
