package com.zqs.grainmall.product.service.impl;

import com.zqs.grainmall.product.entity.AttrEntity;
import com.zqs.grainmall.product.service.AttrService;
import com.zqs.grainmall.product.vo.AttrGroupWithAttrsVo;
import com.zqs.grainmall.product.vo.SkuItemVo;
import com.zqs.grainmall.product.vo.SpuItemAttGroupVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zqs.common.utils.PageUtils;
import com.zqs.common.utils.Query;

import com.zqs.grainmall.product.dao.AttrGroupDao;
import com.zqs.grainmall.product.entity.AttrGroupEntity;
import com.zqs.grainmall.product.service.AttrGroupService;
import org.springframework.util.StringUtils;


@Service("attrGroupService")
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupDao, AttrGroupEntity> implements AttrGroupService {

    @Autowired
    AttrService attrService;



    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrGroupEntity> page = this.page(
                new Query<AttrGroupEntity>().getPage(params),
                new QueryWrapper<AttrGroupEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params, Long catelogId) {
        String key = (String) params.get("key");
        //select * from pms_attr_group where catelog_id=? and (attr_group_id=key or attr_group_name like %key%)
        QueryWrapper<AttrGroupEntity> wrapper = new QueryWrapper<AttrGroupEntity>();
        if (!StringUtils.isEmpty(key))
        {
            wrapper.and((obj)->{
                //查询条件二： attr_group_id=key or attr_group_name=%key%
                obj.eq("attr_group_id",key).or().like("attr_group_name",key);
            });
        }
        //如果传过来的分类id为0,则是查询所有
        if (catelogId==0){
            /* page(x,y):
             *   x- new Query<AttrGroupEntity>(): 分页信息
             *   y- new QueryWrapper<AttrGroupEntity>(): 查询条件
             * */
            IPage<AttrGroupEntity> iPage = this.page(new Query<AttrGroupEntity>().getPage(params),
                                                     wrapper);

            return new PageUtils(iPage);  //返回的json数据看接口文档

        }else {

            wrapper.eq("catelog_id",catelogId);

            IPage<AttrGroupEntity> iPage = this.page(new Query<AttrGroupEntity>().getPage(params),
                    wrapper);

            return new PageUtils(iPage);  //返回的json数据看接口文档

        }

    }

    /**
     *  根据分类id查出所有的分组以及这些组里面的属性
     *
     * */
    @Override
    public List<AttrGroupWithAttrsVo> getAttrGroupWithAttrsByCatelogId(Long catelogId) {

        //1.查询分组信息
        List<AttrGroupEntity> attrGroupEntities = this.list(new QueryWrapper<AttrGroupEntity>().eq("catelog_id", catelogId));

        //2.查询所有属性
        List<AttrGroupWithAttrsVo> collect = attrGroupEntities.stream().map(group -> {
            AttrGroupWithAttrsVo attrsVo = new AttrGroupWithAttrsVo();
            BeanUtils.copyProperties(group, attrsVo);
            List<AttrEntity> attrs = attrService.getRelationAttr(attrsVo.getAttrGroupId());
            attrsVo.setAttrs(attrs);
            return attrsVo;
        }).collect(Collectors.toList());

        //3.


        return collect;
    }

    @Override
    public List<SpuItemAttGroupVo> getAttrGroupWithAttrsBySpuId(Long spuId, Long catalogId) {
        //1.查出当前spu对应的所有属性的分组信息以及当前分组下的所有属性对应的值
        AttrGroupDao baseMapper = this.getBaseMapper();
        List<SpuItemAttGroupVo> vos=baseMapper.getAttrGroupWithAttrsBySpuId(spuId,catalogId);

        //1.1
        return vos;
    }

}