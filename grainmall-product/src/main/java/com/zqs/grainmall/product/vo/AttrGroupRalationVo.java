package com.zqs.grainmall.product.vo;

import lombok.Data;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-10-26 14:43
 **/
@Data
public class AttrGroupRalationVo {
    private Long attrId;

    private Long attrGroupId;
}
