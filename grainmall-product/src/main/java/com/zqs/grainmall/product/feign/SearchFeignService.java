package com.zqs.grainmall.product.feign;

import com.zqs.common.to.es.SkuEsModel;
import com.zqs.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient("grainmall-search")
public interface SearchFeignService {
    @PostMapping("/search/save/product")
    R productStatusUP(@RequestBody List<SkuEsModel> skuEsModels);

}
