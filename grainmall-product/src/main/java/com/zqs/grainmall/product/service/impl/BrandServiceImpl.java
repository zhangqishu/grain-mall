package com.zqs.grainmall.product.service.impl;

import com.zqs.grainmall.product.service.CategoryBrandRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zqs.common.utils.PageUtils;
import com.zqs.common.utils.Query;

import com.zqs.grainmall.product.dao.BrandDao;
import com.zqs.grainmall.product.entity.BrandEntity;
import com.zqs.grainmall.product.service.BrandService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;


@Service("brandService")
public class BrandServiceImpl extends ServiceImpl<BrandDao, BrandEntity> implements BrandService {

    @Autowired
    CategoryBrandRelationService categoryBrandRelationService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        //1.获取key
        String key = (String) params.get("key");
        QueryWrapper<BrandEntity> wrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(key))
        {
            wrapper.eq("brand_id",key).or().like("name",key);
        }


        IPage<BrandEntity> page = this.page(
                new Query<BrandEntity>().getPage(params),
                wrapper);

        return new PageUtils(page);
    }

    @Transactional
    @Override
    public void updateDetaild(BrandEntity brand) {
        //考虑关联表的字段和本表的数据一致
        this.updateById(brand);  //先更新自己表里面的
        if(!StringUtils.isEmpty(brand.getName()))
        {
            //同步更新其他关联表中的数据
            categoryBrandRelationService.updateBrand(brand.getBrandId(),brand.getName());

            //TODO 更新其他关联



        }
    }

    @Override
    public List<BrandEntity> getBrandsByIds(List<Long> brandIds) {


        //in操作符表示可以指定多个值
        return  baseMapper.selectList(new QueryWrapper<BrandEntity>().in("brand_Id",brandIds));
    }

}