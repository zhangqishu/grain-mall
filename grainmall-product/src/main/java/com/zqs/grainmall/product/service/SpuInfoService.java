package com.zqs.grainmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zqs.common.utils.PageUtils;
import com.zqs.grainmall.product.entity.SpuInfoDescEntity;
import com.zqs.grainmall.product.entity.SpuInfoEntity;
import com.zqs.grainmall.product.vo.SpuSaveVo;

import java.util.Map;

/**
 * spu信息
 *
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 15:54:10
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSpuInfo(SpuSaveVo vo);

    void saveBaseSpuInfo(SpuInfoEntity infoEntity);


    PageUtils queryPageByCondition(Map<String, Object> params);


    //商品上架
    void up(Long spuId);

    SpuInfoEntity getSpuInfoBySkuId(Long skuId);

}

