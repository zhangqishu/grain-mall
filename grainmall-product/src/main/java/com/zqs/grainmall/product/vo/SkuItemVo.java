package com.zqs.grainmall.product.vo;

import com.zqs.grainmall.product.entity.SkuImagesEntity;
import com.zqs.grainmall.product.entity.SkuInfoEntity;
import com.zqs.grainmall.product.entity.SpuInfoDescEntity;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-16 10:01
 **/
@ToString
@Data
public class SkuItemVo {

    //1.sku的基本信息获取  pms_sku_info
    SkuInfoEntity info;

    boolean hasStock=true;

    //2.sku的图片信息   pms_sku_images
    List<SkuImagesEntity> images;

    //3.获取spu的销售属性组合
    List<SkuItemSaleAttrVo> saleAttr;

    //4.获取spu的介绍
    SpuInfoDescEntity desp;

    //5.获取spu的规格参数信息
    List<SpuItemAttGroupVo> groupAttrs;






}
