package com.zqs.grainmall.product.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * 商品三级分类
 * 
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 15:54:10
 */
@Data
@TableName("pms_category")
public class CategoryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 分类id
	 */
	@TableId
	private Long catId;
	/**
	 * 分类名称
	 */
	private String name;
	/**
	 * 父分类id
	 */
	private Long parentCid;
	/**
	 * 层级
	 */
	private Integer catLevel;
	/**
	 * 是否显示[0-不显示，1显示]
	 * @TableLogic 表示这是一个逻辑删除字段
	 *     value：逻辑未删除
	 *     delval: 逻辑删除
	 */
	@TableLogic(value = "1",delval = "0")
	private Integer showStatus;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 图标地址
	 */
	private String icon;
	/**
	 * 计量单位
	 */
	private String productUnit;
	/**
	 * 商品数量
	 */
	private Integer productCount;

	/**
	*  子分类，由于这个字段在数据库中没有，所有我们加了@TableField(exist = false)
	* */
	@JsonInclude(JsonInclude.Include.NON_EMPTY)  //当我的children字段不为空的时候才带上，为空的时候就不带这个字段
	@TableField(exist = false)
	private List<CategoryEntity> children;

}
