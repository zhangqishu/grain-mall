package com.zqs.grainmall.product.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-04 11:59
 **/
@AllArgsConstructor
@NoArgsConstructor
@Data
//二级分类vo
public class Catelog2Vo {
    private String catalog1Id;    //一级父分类di
    private List<Catelog3Vo> catalog3List; //三级子分类
    private String id;
    private String name;

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    //三级分类的vo
    public static class Catelog3Vo{
        private String catelog2Id;  //父分类，2级分类id
        private String id;
        private String name;

    }
}
