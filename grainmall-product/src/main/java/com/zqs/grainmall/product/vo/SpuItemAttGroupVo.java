package com.zqs.grainmall.product.vo;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-16 13:50
 **/
@ToString
@Data
public class SpuItemAttGroupVo {
    private String groupName;
    private List<Attr> attrs;
}
