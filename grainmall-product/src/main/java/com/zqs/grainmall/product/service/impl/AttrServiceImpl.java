package com.zqs.grainmall.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.zqs.common.constant.ProductConstant;
import com.zqs.grainmall.product.dao.AttrAttrgroupRelationDao;
import com.zqs.grainmall.product.dao.AttrGroupDao;
import com.zqs.grainmall.product.dao.CategoryDao;
import com.zqs.grainmall.product.entity.AttrAttrgroupRelationEntity;
import com.zqs.grainmall.product.entity.AttrGroupEntity;
import com.zqs.grainmall.product.entity.CategoryEntity;
import com.zqs.grainmall.product.service.CategoryService;
import com.zqs.grainmall.product.vo.AttrGroupRalationVo;
import com.zqs.grainmall.product.vo.AttrGroupWithAttrsVo;
import com.zqs.grainmall.product.vo.AttrRespVo;
import com.zqs.grainmall.product.vo.AttrVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zqs.common.utils.PageUtils;
import com.zqs.common.utils.Query;

import com.zqs.grainmall.product.dao.AttrDao;
import com.zqs.grainmall.product.entity.AttrEntity;
import com.zqs.grainmall.product.service.AttrService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;


@Service("attrService")
public class AttrServiceImpl extends ServiceImpl<AttrDao, AttrEntity> implements AttrService {

    @Autowired
    AttrAttrgroupRelationDao attrAttrgroupRelationDao;

    @Autowired
    AttrGroupDao attrGroupDao;

    @Autowired
    CategoryDao categoryDao;

    @Autowired
    CategoryService categoryService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrEntity> page = this.page(
                new Query<AttrEntity>().getPage(params),
                new QueryWrapper<AttrEntity>()
        );

        return new PageUtils(page);
    }

    @Transactional
    @Override
    public void saveAttr(AttrVo attr) {
        AttrEntity attrEntity = new AttrEntity();
     //   attrEntity.setAttrName(attr.getAttrName());
        BeanUtils.copyProperties(attr,attrEntity);  //将attr里面的值复制到attrEntity里面
        //1.保存基本数据
        this.save(attrEntity);
        //2.保存关联关系
        if (attr.getAttrType()== ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode()&&attr.getAttrGroupId()!=null)
        {
            AttrAttrgroupRelationEntity relationEntity = new AttrAttrgroupRelationEntity();
            relationEntity.setAttrGroupId(attr.getAttrGroupId());
            relationEntity.setAttrId(attrEntity.getAttrId());
            attrAttrgroupRelationDao.insert(relationEntity);
        }

    }

    @Override
    public PageUtils queryBaseAttrPage(Map<String, Object> params, Long catelogId, String type) {
        QueryWrapper<AttrEntity> wrapper = new QueryWrapper<AttrEntity>()
                .eq("attr_type","base".equalsIgnoreCase(type)?ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode():ProductConstant.AttrEnum.ATTR_TYPE_SALE.getCode());
        if (catelogId!=0)
        {
            wrapper.eq("catelog_id",catelogId);
        }
        String value=(String) params.get("key");
        if (!StringUtils.isEmpty(value))
        {
            //attr_id attr_name
            wrapper.and((x)->{
                x.eq("attr_id",value).or().like("attr_name",value);
            });
        }

        IPage<AttrEntity> page = this.page(new Query<AttrEntity>().getPage(params), wrapper);

        PageUtils pageUtils=new PageUtils(page);


        //getRecords(): 得到当前分页记录
        List<AttrEntity> records = page.getRecords();
        //为响应对象添加GroupName和CateLogName
        List<AttrRespVo> respVos=records.stream().map((attrEntity)->{
           AttrRespVo attrRespVo= new AttrRespVo();

           BeanUtils.copyProperties(attrEntity,attrRespVo);

           //1.设置分类和分组的名字
            if ("base".equalsIgnoreCase(type))
            {
                AttrAttrgroupRelationEntity attrId=attrAttrgroupRelationDao.selectOne(new QueryWrapper<AttrAttrgroupRelationEntity>()
                        .eq("attr_id",attrEntity.getAttrId()));

                if (attrId!=null&&attrId.getAttrGroupId()!=null)
                {
                    AttrGroupEntity attrGroupEntity=attrGroupDao.selectById(attrId.getAttrGroupId());
                    attrRespVo.setGroupName(attrGroupEntity.getAttrGroupName());
                }

            }


          CategoryEntity categoryEntity= categoryDao.selectById(attrEntity.getCatelogId());
           if (categoryEntity!=null)
           {
               attrRespVo.setCateLogName(categoryEntity.getName());
           }

            return attrRespVo;
        }).collect(Collectors.toList());

        //设置为最新分页数据
        pageUtils.setList(respVos);

        return pageUtils;
    }


    @Cacheable(value = "attr",key = "'attrinfo:'+#root.args[0]")
    @Override
    public AttrRespVo getAttrInfo(Long attrId) {
        AttrRespVo respVo = new AttrRespVo();
        AttrEntity attrEntity = this.getById(attrId);
        BeanUtils.copyProperties(attrEntity,respVo);

        if (attrEntity.getAttrType()==ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode())
        {
            //1.设置分组信息
            AttrAttrgroupRelationEntity attrgroupRelationEntity= attrAttrgroupRelationDao.selectOne(new QueryWrapper<AttrAttrgroupRelationEntity>().
                    eq("attr_id",attrId));
            if (attrgroupRelationEntity!=null)
            {
                respVo.setAttrGroupId(attrgroupRelationEntity.getAttrGroupId());
                AttrGroupEntity attrGroupEntity = attrGroupDao.selectById(attrgroupRelationEntity.getAttrGroupId());
                if (attrGroupEntity!=null)
                {
                    respVo.setGroupName(attrGroupEntity.getAttrGroupName());
                }
            }

        }



       //2.设置分类信息
        Long catelogId = attrEntity.getCatelogId();

        Long[] catelogPath = categoryService.findCatelogPath(catelogId);
        respVo.setCatelogPath(catelogPath);

        CategoryEntity categoryEntity = categoryDao.selectById(catelogId);
        if (categoryEntity!=null)
        {
            respVo.setCateLogName(categoryEntity.getName());

        }


        return respVo;
    }

    @Transactional
    @Override
    public void updateAttr(AttrVo attr) {
        AttrEntity attrEntity = new AttrEntity();
        BeanUtils.copyProperties(attr,attrEntity);
        this.updateById(attrEntity);

        if (attrEntity.getAttrType()==ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode()) {
            //1.修改分组关联
            AttrAttrgroupRelationEntity relationEntity = new AttrAttrgroupRelationEntity();

            relationEntity.setAttrGroupId(attr.getAttrGroupId());
            relationEntity.setAttrId(attr.getAttrId());

            Integer count = attrAttrgroupRelationDao.selectCount(new QueryWrapper<AttrAttrgroupRelationEntity>()
                    .eq("attr_id", attr.getAttrId()));


            if (count > 0) {
                attrAttrgroupRelationDao.update(relationEntity, new UpdateWrapper<AttrAttrgroupRelationEntity>()
                        .eq("attr_id", attr.getAttrId()));

            } else {
                attrAttrgroupRelationDao.insert(relationEntity);
            }

        }


        ;




    }

    /**
     * 根据分组id来找到这个组内关联的所有属性
     *
     * */
    @Override
    public List<AttrEntity> getRelationAttr(Long attrgroupId) {
        List<AttrAttrgroupRelationEntity> entities = attrAttrgroupRelationDao.selectList(new QueryWrapper<AttrAttrgroupRelationEntity>()
                .eq("attr_group_id", attrgroupId));

        List<Long> attrIds = entities.stream().map((attr) -> attr.getAttrId()).collect(Collectors.toList());

        if(attrIds==null||attrIds.size()==0)
        {
            return null;
        }
        Collection<AttrEntity> attrEntities = this.listByIds(attrIds);

        return (List<AttrEntity>) attrEntities;
    }

    @Override
    public void deleteRelation(AttrGroupRalationVo[] vos) {
       // attrAttrgroupRelationDao.delete(new QueryWrapper<>().eq("attr_id",1L).eq("attr_group",1L));
        //批量删除

        List<AttrAttrgroupRelationEntity> entities = Arrays.asList(vos).stream().map((item) -> {
            AttrAttrgroupRelationEntity relationEntity = new AttrAttrgroupRelationEntity();
            BeanUtils.copyProperties(item, relationEntity);
            return relationEntity;
        }).collect(Collectors.toList());
        attrAttrgroupRelationDao.deleteBatchRalation(entities);


    }

    /**
     *  获取当前分组没有关联的所有属性
     *
     *
     * */
    @Override
    public PageUtils getNoRelationAttr(Map<String, Object> params, Long attrgroupId) {
        //1.当前分组只能关联自己所属的分类里面的所有属性

        AttrGroupEntity attrGroupEntity = attrGroupDao.selectById(attrgroupId);
        Long catelogId=attrGroupEntity.getCatelogId();

        //2.当前分组只能关联别的分组没有引用的属性
         //2.1 当前分类的其他分组
        List<AttrGroupEntity> group = attrGroupDao.selectList(new QueryWrapper<AttrGroupEntity>()
                .eq("catelog_id", catelogId));

        List<Long> collect = group.stream().map(item -> item.getAttrGroupId()).collect(Collectors.toList());

        //2.2 这些分组关联的属性
        List<AttrAttrgroupRelationEntity> groupId = attrAttrgroupRelationDao.selectList(new QueryWrapper<AttrAttrgroupRelationEntity>().in("attr_group_id", collect));

        List<Long> attrIds = groupId.stream().map(item -> item.getAttrId()).collect(Collectors.toList());


        //2.3 从当前分类的所有属性中移除这些属性
        QueryWrapper<AttrEntity> wrapper = new QueryWrapper<AttrEntity>().eq("catelog_id", catelogId).eq("attr_type",ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode());
        if (attrIds!=null&&attrIds.size()>0)
        {
            wrapper.notIn("attr_id", attrIds);
        }
        String value = (String) params.get("key");
        if (!StringUtils.isEmpty(value))
        {
            wrapper.and((w)->w.eq("attr_id",value).or().like("attr_name",value));

        }
        IPage<AttrEntity> page = this.page(new Query<AttrEntity>().getPage(params), wrapper);

        PageUtils pageUtils = new PageUtils(page);
        return pageUtils;
    }

    @Override
    public List<Long> selectSearchAttrIds(List<Long> attrIds) {
        //select  attr_id  from `pms_attr` where attr_id in (?) and search_type=1

       List<Long> ids= baseMapper.selectSearchAttrIds(attrIds);

        return ids;
    }

}