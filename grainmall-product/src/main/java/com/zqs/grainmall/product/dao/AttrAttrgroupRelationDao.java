package com.zqs.grainmall.product.dao;

import com.zqs.grainmall.product.entity.AttrAttrgroupRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 属性&属性分组关联
 * 
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 15:54:10
 */
@Mapper
public interface AttrAttrgroupRelationDao extends BaseMapper<AttrAttrgroupRelationEntity> {

    void deleteBatchRalation(@Param("entities") List<AttrAttrgroupRelationEntity> entities);

}
