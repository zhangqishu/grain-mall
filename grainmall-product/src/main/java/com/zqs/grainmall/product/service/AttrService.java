package com.zqs.grainmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zqs.common.utils.PageUtils;
import com.zqs.grainmall.product.entity.AttrEntity;
import com.zqs.grainmall.product.vo.AttrGroupRalationVo;
import com.zqs.grainmall.product.vo.AttrGroupWithAttrsVo;
import com.zqs.grainmall.product.vo.AttrRespVo;
import com.zqs.grainmall.product.vo.AttrVo;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 15:54:11
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveAttr(AttrVo attr);


    PageUtils queryBaseAttrPage(Map<String, Object> params, Long catelogId, String type);

    AttrRespVo getAttrInfo(Long attrId);

    void updateAttr(AttrVo attr);

    List<AttrEntity> getRelationAttr(Long attrgroupId);

    void deleteRelation(AttrGroupRalationVo[] vos); //option+command+b

    PageUtils getNoRelationAttr(Map<String, Object> params, Long attrgroupId);


    /**
     *
     * 在指定的所有属性集合里面，挑出检索属性
     * */
    List<Long> selectSearchAttrIds(List<Long> attrIds);

}

