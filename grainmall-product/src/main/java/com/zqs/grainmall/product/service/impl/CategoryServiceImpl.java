package com.zqs.grainmall.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.zqs.grainmall.product.service.CategoryBrandRelationService;
import com.zqs.grainmall.product.vo.Catelog2Vo;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zqs.common.utils.PageUtils;
import com.zqs.common.utils.Query;

import com.zqs.grainmall.product.dao.CategoryDao;
import com.zqs.grainmall.product.entity.CategoryEntity;
import com.zqs.grainmall.product.service.CategoryService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

//    @Autowired
//    CategoryDao categoryDao;
//    categoryDao相当于baseMapper

    @Autowired
    CategoryBrandRelationService categoryBrandRelationService;

    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    RedissonClient redissonClient;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        //1.查出所有分类
        List<CategoryEntity> entities = baseMapper.selectList(null);


        //2.组装成父子的树型结构

        //2.1 找到所有的一级分类
        List<CategoryEntity> level1Menus = entities.stream()
                .filter((categoryEntity) -> categoryEntity.getParentCid() == 0)  //过滤出parentID为0的数据
                .map((menu)->{
                    //把处理好的数据放到children集合中，menu:当前的，entities:所有的数据
                    menu.setChildren(getChildrens(menu,entities));
                    return menu;
                })
                //根据sorted字段进行排序
                .sorted((menu1,menu2)->{
                    return (menu1.getSort()==null?0:menu1.getSort())-(menu2.getSort()==null?0:menu2.getSort());
                })
                .collect(Collectors.toList());



        return level1Menus;
    }

    /**
     *  递归查找所有菜单的子菜单
     *  CategoryEntity 为当前菜单
     *  List<CategoryEntity> 为所有菜单
     * */
    private List<CategoryEntity> getChildrens(CategoryEntity root,List<CategoryEntity> all){
        List<CategoryEntity> children = all.stream()
                .filter(categoryEntity ->
                {
                    return categoryEntity.getParentCid() == root.getCatId(); //过滤出parentCid与CatId相等的
                })
                .map(categoryEntity ->
                {
                    //1.找到子菜单
                    categoryEntity.setChildren(getChildrens(categoryEntity,all));
                    return categoryEntity;
                })
                .sorted((menu1,menu2)->{
                    return (menu1.getSort()==null?0:menu1.getSort())-(menu2.getSort()==null?0:menu2.getSort());
                })
                .collect(Collectors.toList());


        return children;


    }


    @Override
    public void removeMenuByIds(List<Long> asList) {
        //TODO 1.检查当前删除的菜单，是否为别的地方引用

        //逻辑删除

        baseMapper.deleteBatchIds(asList);

    }

    //[2,25,225]
    @Override
    public Long[] findCatelogPath(Long catelogId) {
        List<Long> paths=new ArrayList<>();  //用来收集id
        List<Long> parentPath=findParentPath(catelogId,paths);

        Collections.reverse(parentPath); //倒序


        return parentPath.toArray(new Long[parentPath.size()]); //将集合转化为数组
    }

    //225,25,2
    private List<Long> findParentPath(Long catelogId,List<Long> paths){
        //1.收集当前节点id
        paths.add(catelogId);
        CategoryEntity byId=this.getById(catelogId);
        if(byId.getParentCid()!=0)
        {
            findParentPath(byId.getParentCid(),paths);

        }
        return paths;

    }

    /**
     *  级联更新所有关联的数据
     *
     * @CacheEvict: 失效模式:也就是说我如果修改了菜单数据，会自动把getLevel1Categorys这个缓存删除
     *              value = "category",allEntries = true 删除category这个分区下的所有数据
     * @Caching: 同时进行多种缓存操作
     *
     * @CachePut: 双写模式
     *
     * 总结: 存储同一类型的数据，都可以指定成同一个分区，分区名默认就是缓存的前缀
     * */


//    @Caching(evict = {
//            @CacheEvict(value = "category",key = "'getLevel1Categorys'"),
//            @CacheEvict(value = "category",key = "'getCatelogJson'")
//    })
    @CacheEvict(value = "category",allEntries = true)
    @Transactional
    @Override
    public void updateCascade(CategoryEntity category) {
        this.updateById(category); //先更新自己
        categoryBrandRelationService.updateCategory(category.getCatId(),category.getName());

        //同时修改缓存中的数据
        //redis.delete("catalogJSON"); 等待下次主动查询进行更新

    }


    /**
     *
     * 每一个需要缓存的数据我们都来指定要放到哪个名字的缓存。【缓存分区(按照类型分)】
     * 默认行为:
     *   1.如果缓存中有,方法不用调用
     *   2.key是默认自动生成的  category::SimpleKey []
     *   3.缓存value的值，默认使用jdk序列化机制，将序列化后的数据存到redis
     *   4.默认ttl时间是-1:
     *
     * 自定义操作:
     *   1.指定生成的缓存使用的key  key属性指定，接受一个SpEL
     *   2.指定缓存的数据存活时间   配置文件中修改ttl
     *   3.将数据保存为json格式
     *
     *
     *   4.原理： CacheAutoConfiguration--> RedisCacheConfiguration-->
     *           自动配置了缓存管理器RedisCacheManager-->初始化所有的缓存-->
     *           每个缓存决定使用什么配置-->如果redisCacheConfiguration有就用已有的，没有就用默认的-->
     *           想改缓存中的配置，只需要给容器中放一个RedisCacheConfiguration即可-->
     *           就会应用到当前RedisCacheManager管理的所有缓存分区中
     */
    @Cacheable(value = {"category"},key = "#root.method.name",sync = true)   //表示当前方法的结果需要缓存，如果缓存中有，方法不用调用，如果缓存中没有，会调用方法，最后将方法的结果放入缓存
    @Override
    public List<CategoryEntity> getLevel1Categorys() {
        System.out.println("getLevel1Categorys.....");
        List<CategoryEntity> parentCid0 = baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_cid", 0));

        return parentCid0;
    }

    //TODO 使用redisson来实现
    //那么问题又来了，如何保证缓存里面的数据和数据库保持一致呢？数据一致性问题
    //1.双写模式  也就是在数据库更新的时候加上redis的更新操作  容易出现脏读的问题,如何解决？加锁，只有当前线程执行完了后面才可以执行
    //2.失效模式  将数据库改完之后，我们把缓存删了,下次就会重新查数据库，主动更新缓存
    //TODO 对于并发几率小的(订单数据，用户数据)，不用考虑数据一致性问题，给缓存数据加过期时间，每隔一段时间触发读的主动更新即可
    // 如果是像菜单，商品介绍等基础数据也可以不考虑数据一致性问题，可以使用canal订阅binlog的方式
    // 缓存数据+过期时间也足够解决大部分业务对于缓存的要求，因为最终数据也会更新，只要不是要求强一致的数据，都可以使用加过期时间解决
    // 通过加读写锁来保证并发读写防止脏读，写写的时候排好队，读读无所谓

    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDbWithRedissonLock() {

        //1.占分布式锁,锁的名字-->锁的粒度，越细越快
        //锁的粒度:具体缓存的是某个数据，11-号商品  product-11-lock
        RLock lock = redissonClient.getLock("catalogJson-lock");
        lock.lock();
        Map<String, List<Catelog2Vo>> dataFromDb = null;
        try {
            dataFromDb = getDataFromDb();
        }finally {
            lock.unlock();
        }
        return dataFromDb;

    }





    //TODO redis分布式锁原理 158
    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDbWithRedisLock() {
        //redis核心有2处，1.加锁保证原子性 2.解锁保证原子性
        //加锁使用set ??? nx Ex ???  命令
        //解锁不要设置固定的字符串，要给一个随机的大字符串，比如uuid,通过脚本进行解锁
        //1.占分布式锁。去redis占坑
        String uuid = UUID.randomUUID().toString();
        //原子操作
        Boolean lock=redisTemplate.opsForValue().setIfAbsent("lock",uuid,300,TimeUnit.SECONDS);
            if (lock)
            {
                System.out.println("获取分布式锁成功.....");
                //加锁成功....执行业务
                //2.设置过期时间，必须和加锁是同步的
                //redisTemplate.expire("lock",30,TimeUnit.SECONDS); //设置过期时间为30秒
                Map<String, List<Catelog2Vo>> dataFromDb=null;
                try {
                    dataFromDb = getDataFromDb();
                }finally {
                    //1.获取值对比，2.对比成功后删除。这2步必须是一个原子操作  如何实现？Lua脚本解锁
                    String script="if redis.call(\"get\",KEYS[1]) == ARGV[1]\n" +
                            "then\n" +
                            "    return redis.call(\"del\",KEYS[1])\n" +
                            "else\n" +
                            "    return 0\n" +
                            "end";
                    //lua脚本删除锁
                    Long lock1 = redisTemplate.execute(new DefaultRedisScript<Long>(script, Long.class), Arrays.asList("lock"), uuid);

                }

//                String lockValue = redisTemplate.opsForValue().get("lock");
//                if (lockValue.equals(uuid))
//                {
//                    redisTemplate.delete("lock");  //执行业务后删除锁
//                }

                return dataFromDb;
            }
            else {
                //加锁失败.....重试。synchronized()
                //休眠100ms后重试
                System.out.println("获取分布式锁失败....等待重试");
                try {
                    Thread.sleep(200);
                }catch (Exception e){

                }
                return getCatalogJsonFromDbWithRedisLock(); //自旋的方式

            }


    }

    private Map<String, List<Catelog2Vo>> getDataFromDb() {
        String catalogJson = redisTemplate.opsForValue().get("catalogJson");
        if (!StringUtils.isEmpty(catalogJson)) {
            //缓存不为null直接返回
            Map<String, List<Catelog2Vo>> result = JSON.parseObject(catalogJson, new TypeReference<Map<String, List<Catelog2Vo>>>() {
            });

            return result;
        }
        System.out.println("查询了数据库.....");

        List<CategoryEntity> selectList = baseMapper.selectList(null);

        List<CategoryEntity> level1Categorys = getParent_cid(selectList, 0L);

        //2.封装数据
        Map<String, List<Catelog2Vo>> map = level1Categorys.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            //1.每一个一级分类，查到这个一级分类的二级分类
            List<CategoryEntity> categoryEntities = getParent_cid(selectList, v.getCatId());
            //2.封装上面的结果
            List<Catelog2Vo> catelog2Vos = null;
            if (categoryEntities != null) {
                catelog2Vos = categoryEntities.stream().map(l2 -> {
                    Catelog2Vo catelog2Vo = new Catelog2Vo(v.getCatId().toString(), null, l2.getCatId().toString(), l2.getName());
                    //1.找当前二级分类的三级分类封装成vo
                    List<CategoryEntity> leve3Catelog = getParent_cid(selectList, l2.getCatId());
                    if (leve3Catelog != null) {
                        List<Catelog2Vo.Catelog3Vo> collect = leve3Catelog.stream().map(l3 -> {
                            //2.封装成指定格式
                            Catelog2Vo.Catelog3Vo catelog3Vo = new Catelog2Vo.Catelog3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());
                            return catelog3Vo;
                        }).collect(Collectors.toList());

                        catelog2Vo.setCatalog3List(collect);

                    }
                    return catelog2Vo;
                }).collect(Collectors.toList());

            }
            return catelog2Vos;
        }));


        //3.把查到的数据再放入缓存-->将对象转为json放到缓存中
        String s = JSON.toJSONString(map);
        redisTemplate.opsForValue().set("catalogJson", s, 1, TimeUnit.DAYS);  //设置一天之后过期

        return map;
    }

    /**
     *
     * 查出二级三级分类的数据返回
     *
     * */
    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDbWithLocalLock() {

        //加锁，只要是同一把锁，就能锁住需要这个锁的所有线程
        //1.synchronized (this)： Springboot所有的组件在容器中都是单例的
        //TODO 本地锁:synchronized,JUC(lock),在分布式情况下，想要锁住所有，必须使用分布式锁
        synchronized (this)
        {
            //得到锁以后，我们应该再去缓存中确定一次，如果没有才需要继续查询
            return getDataFromDb();

        }

    }

    @Cacheable(value = "category",key = "#root.methodName")
    @Override
    public Map<String, List<Catelog2Vo>> getCatelogJson() {
        System.out.println("查询了数据库.....");

        List<CategoryEntity> selectList = baseMapper.selectList(null);

        List<CategoryEntity> level1Categorys = getParent_cid(selectList, 0L);

        //2.封装数据
        Map<String, List<Catelog2Vo>> map = level1Categorys.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            //1.每一个一级分类，查到这个一级分类的二级分类
            List<CategoryEntity> categoryEntities = getParent_cid(selectList, v.getCatId());
            //2.封装上面的结果
            List<Catelog2Vo> catelog2Vos = null;
            if (categoryEntities != null) {
                catelog2Vos = categoryEntities.stream().map(l2 -> {
                    Catelog2Vo catelog2Vo = new Catelog2Vo(v.getCatId().toString(), null, l2.getCatId().toString(), l2.getName());
                    //1.找当前二级分类的三级分类封装成vo
                    List<CategoryEntity> leve3Catelog = getParent_cid(selectList, l2.getCatId());
                    if (leve3Catelog != null) {
                        List<Catelog2Vo.Catelog3Vo> collect = leve3Catelog.stream().map(l3 -> {
                            //2.封装成指定格式
                            Catelog2Vo.Catelog3Vo catelog3Vo = new Catelog2Vo.Catelog3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());
                            return catelog3Vo;
                        }).collect(Collectors.toList());

                        catelog2Vo.setCatalog3List(collect);

                    }
                    return catelog2Vo;
                }).collect(Collectors.toList());

            }
            return catelog2Vos;
        }));
        return map;
    }

    // TODO 产生堆外内存溢出异常: OutOfDirectMemoryError
    // 1.springboot2.0以后默认使用lettuce作为redis的客户端。它使用的是netty进行网络通信
    // 2.lettuce的bug导致netty堆外内存溢出 这个服务jvm参数是: -Xmx300m,netty如果没有指定堆外内存，默认使用-Xmx300m
    // 解决方案:
    // 不能使用-Dio.netty.maxDirectMemory只能调大堆外内存
    // 1.升级lettuce客户端
    // 2.切换使用jedis  目前的是切换jedis的客户端，然后解决，上线后我们再解决
    // 总结:redisTemplate是Spring对redis的又一层封装，无论你的底层用lettuce或者jedis都可以兼容
    public Map<String,List<Catelog2Vo>> getCatelogJson2(){
        //思路: 给缓存中放json字符串，拿出的json字符串，还能逆转为能用的对象类型；【序列化与反序列化】

        /**
         * 1.空结果缓存: 解决缓存穿透
         * 2.设置过期时间(加随机值);解决缓存雪崩
         * 3.加锁:解决缓存击穿
         *
         * */

        //1.加入缓存逻辑，缓存中存的数据是json字符串
        //JSON跨语言，跨平台兼容的
        String catalogJson = redisTemplate.opsForValue().get("catalogJson");
        if (StringUtils.isEmpty(catalogJson))
        {
            //2.缓存中没有数据,查数据库
            System.out.println("缓存没有命中....将要查询数据库....");
            Map<String, List<Catelog2Vo>> catalogJsonFromDb = getCatalogJsonFromDbWithRedisLock();

            return catalogJsonFromDb;

        }
        System.out.println("缓存命中...直接返回.....");

        //再把json字符串转为我们指定的对象。
        Map<String, List<Catelog2Vo>> result=JSON.parseObject(catalogJson,new TypeReference< Map<String, List<Catelog2Vo>>>(){});

        return result;

    }




    private List<CategoryEntity> getParent_cid(List<CategoryEntity> selectList,Long parent_cid) {
        List<CategoryEntity> collect = selectList.stream()
                .filter(item -> item.getParentCid() == parent_cid)
                .collect(Collectors.toList());
        // return baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_cid", v.getCatId()));
        return collect;
    }


}