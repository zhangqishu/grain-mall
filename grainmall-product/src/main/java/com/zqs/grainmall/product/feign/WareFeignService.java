package com.zqs.grainmall.product.feign;

import com.zqs.common.to.SkuHasStockVo;
import com.zqs.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient("grainmall-ware")
public interface WareFeignService {

    /**
     * json-->对象 3种设计方案
     * 1.R 设计的时候加上范型
     * 2.直接返回我们想要的结果
     * 3.自己封装解析结果
     * */
    @PostMapping("/ware/waresku/hasstock")
    R getSkuHasStock(@RequestBody List<Long> skuIds);


}
