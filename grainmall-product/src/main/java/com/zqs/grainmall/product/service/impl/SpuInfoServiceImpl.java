package com.zqs.grainmall.product.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.zqs.common.constant.ProductConstant;
import com.zqs.common.to.SkuHasStockVo;
import com.zqs.common.to.SkuReductionTo;
import com.zqs.common.to.SpuBoundTo;
import com.zqs.common.to.es.SkuEsModel;
import com.zqs.common.utils.R;
import com.zqs.grainmall.product.entity.*;
import com.zqs.grainmall.product.feign.CouponFeignService;
import com.zqs.grainmall.product.feign.SearchFeignService;
import com.zqs.grainmall.product.feign.WareFeignService;
import com.zqs.grainmall.product.service.*;
import com.zqs.grainmall.product.vo.*;
//import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zqs.common.utils.PageUtils;
import com.zqs.common.utils.Query;

import com.zqs.grainmall.product.dao.SpuInfoDao;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;


@Service("spuInfoService")
public class SpuInfoServiceImpl extends ServiceImpl<SpuInfoDao, SpuInfoEntity> implements SpuInfoService {

    @Autowired
    SpuInfoDescService spuInfoDescService;

    @Autowired
    SpuImagesService spuImagesService;

    @Autowired
    AttrService attrService;

    @Autowired
    ProductAttrValueService productAttrValueService;

    @Autowired
    SkuInfoService skuInfoService;

    @Autowired
    SkuImagesService skuImagesService;

    @Autowired
    SkuSaleAttrValueService skuSaleAttrValueService;

    @Autowired
    CouponFeignService couponFeignService;

    @Autowired
    BrandService brandService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    WareFeignService wareFeignService;

    @Autowired
    SearchFeignService searchFeignService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SpuInfoEntity> page = this.page(
                new Query<SpuInfoEntity>().getPage(params),
                new QueryWrapper<SpuInfoEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * TODO 这个方法仔细想想,目前bug很多，高级部分再优化
     * TODO 高级部分: 这个业务不要求很高的并发，适合使用AT模式
     * */
    //Seata AT 分布式事务  @GlobalTransactional

    @Transactional
    @Override
    public void saveSpuInfo(SpuSaveVo vo) {


        //1.保存spu基本信息    pms_spu_info
        SpuInfoEntity infoEntity = new SpuInfoEntity();
        BeanUtils.copyProperties(vo,infoEntity);
        infoEntity.setCreateTime(new Date());
        infoEntity.setUpdateTime(new Date());

        this.saveBaseSpuInfo(infoEntity);

        //2.保存Spu的描述图片   pms_spu_info_desc
        List<String> decript=vo.getDecript();
        SpuInfoDescEntity descEntity = new SpuInfoDescEntity();
        descEntity.setSpuId(infoEntity.getId());
        descEntity.setDecript(String.join(",",decript));
        spuInfoDescService.saveSpuInfoDesc(descEntity);

        //3.保存spu的图片集     pms_spu_images
        List<String> images = vo.getImages();
        spuImagesService.saveImages(infoEntity.getId(),images);


        //4.保存spu的规格参数   pms_product_attr_value
        List<BaseAttrs> baseAttrs = vo.getBaseAttrs();
        List<ProductAttrValueEntity> collect = baseAttrs.stream().map(attr -> {
            ProductAttrValueEntity valueEntity = new ProductAttrValueEntity();
            valueEntity.setAttrId(attr.getAttrId());
            AttrEntity id = attrService.getById(attr.getAttrId());
            valueEntity.setAttrName(id.getAttrName());
            valueEntity.setAttrValue(attr.getAttrValues());
            valueEntity.setQuickShow(attr.getShowDesc());
            valueEntity.setSpuId(infoEntity.getId());

            return valueEntity;

        }).collect(Collectors.toList());

        productAttrValueService.saveProductAttr(collect);

        //5、保存spu的积分信息: gulimall_sms->sms_spu_bounds
        Bounds bounds = vo.getBounds();
        SpuBoundTo spuBoundTo = new SpuBoundTo();
        BeanUtils.copyProperties(bounds,spuBoundTo);
        spuBoundTo.setSpuId(infoEntity.getId());
        R r = couponFeignService.saveSpuBounds(spuBoundTo);
        if (r.getCode()!=0)
        {
            log.error("远程保存spu积分信息失败");

        }


        //5.保存当前spu对应的所有sku信息

        List<Skus> skus = vo.getSkus();
        if (skus!=null&&skus.size()>0) {
            skus.forEach(item->{
                String defaultImg="";

                for (Images image : item.getImages()) {
                    if (image.getDefaultImg()==1)
                    {
                        defaultImg=image.getImgUrl();

                    }

                }
                //  private List<Attr> attr;
                //  private String skuName;
                //  private BigDecimal price;
                //  private String skuTitle;
                //  private String skuSubtitle;
                SkuInfoEntity skuInfoEntity = new SkuInfoEntity();
                BeanUtils.copyProperties(item,skuInfoEntity);
                skuInfoEntity.setBrandId(infoEntity.getBrandId());
                skuInfoEntity.setCatalogId(infoEntity.getCatalogId());
                skuInfoEntity.setSaleCount(0L);
                skuInfoEntity.setSpuId(infoEntity.getId());
                skuInfoEntity.setSkuDefaultImg(defaultImg);

                //5.1、 sku的基本信息; pms_sku_info
                skuInfoService.saveSkuInfo(skuInfoEntity);


                Long skuId = skuInfoEntity.getSkuId();

                List<SkuImagesEntity> imagesEntities = item.getImages().stream().map(img -> {
                    SkuImagesEntity skuImagesEntity = new SkuImagesEntity();
//                    if (img.getDefaultImg()==1)
//                    {
//                        skuInfoEntity.setSkuDefaultImg();
//                    }
                    skuImagesEntity.setSkuId(skuId);
                    skuImagesEntity.setImgUrl(img.getImgUrl());
                    skuImagesEntity.setDefaultImg(img.getDefaultImg());
                    return skuImagesEntity;
                }).filter(entity->{
                    //返回true就是需要，false就是剔除
                    return !StringUtils.isEmpty(entity.getImgUrl());
                }).collect(Collectors.toList());


                //5.2、 sku的图片信息; pms_sku_images

                skuImagesService.saveBatch(imagesEntities);

                //5.3、 sku的销售属性信息; pms_sku_sale_attr_value
                List<Attr> attr = item.getAttr();
                List<SkuSaleAttrValueEntity> skuSaleAttrValueEntities = attr.stream().map(a -> {
                    SkuSaleAttrValueEntity attrValueEntity = new SkuSaleAttrValueEntity();
                    BeanUtils.copyProperties(a, attrValueEntity);
                    attrValueEntity.setSkuId(skuId);
                    return attrValueEntity;
                }).collect(Collectors.toList());

                skuSaleAttrValueService.saveBatch(skuSaleAttrValueEntities);


                //5.4、 sku的优惠、满减等信息; gulimall_sms->sms_sku_ladder\sms_sku_full_reduction\sms_member_price

                SkuReductionTo skuReductionTo=new SkuReductionTo();
                BeanUtils.copyProperties(item,skuReductionTo);
                skuReductionTo.setSkuId(skuId);
                if (skuReductionTo.getFullCount()>0||skuReductionTo.getFullPrice().compareTo(new BigDecimal("0"))==1)
                {
                    R r1 = couponFeignService.saveSkuReduction(skuReductionTo);

                    if (r1.getCode()!=0)
                    {
                        log.error("远程保存sku优惠信息失败");

                    }

                }


            });
        }




        //





    }

    @Override
    public void saveBaseSpuInfo(SpuInfoEntity infoEntity) {
        this.baseMapper.insert(infoEntity);

    }

    @Override
    public PageUtils queryPageByCondition(Map<String, Object> params) {
        QueryWrapper<SpuInfoEntity> wrapper = new QueryWrapper<>();
        String key = (String) params.get("key");
        if (!StringUtils.isEmpty(key))
        {
            wrapper.and((w)->{
                w.eq("id",key).or().like("spu_name",key);
            });


        }
        String status = (String) params.get("status");
        if (!StringUtils.isEmpty(status))
        {
            wrapper.eq("publish_status",status);


        }
        String brandId = (String) params.get("brandId");
        if (!StringUtils.isEmpty(brandId)&&!"0".equalsIgnoreCase(brandId))
        {
            wrapper.eq("brand_id",brandId);


        }
        String catelogId = (String) params.get("catelogId");
        if (!StringUtils.isEmpty(catelogId)&&!"0".equalsIgnoreCase(catelogId))
        {
            wrapper.eq("catalog_id",catelogId);


        }


        IPage<SpuInfoEntity> page = this.page(
                new Query<SpuInfoEntity>().getPage(params),
                wrapper
        );
        return new PageUtils(page);
    }

    /**
     * 商品上架
     * TODO 130～135集再看看
     *
     * */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void up(Long spuId) {

        //1.查出当前spuId对应的所有sku信息，品牌的名字
        List<SkuInfoEntity> skuInfoEntities=skuInfoService.getSkusBySpuId(spuId);
        List<Long> skuIdList = skuInfoEntities.stream()
                .map(SkuInfoEntity::getSkuId)
                .collect(Collectors.toList());


        //TODO 4.查询当前sku的所有可以被用来检索的规格属性
        List<ProductAttrValueEntity> baseAttrs= productAttrValueService.baseAttrlistforspu(spuId);

        //把baseAttrs中所有的AttrId抽取出来封装成一个list
        List<Long> attrIds = baseAttrs.stream().map(x -> {
            return x.getAttrId();
        }).collect(Collectors.toList());


       List<Long> searchAttrIds= attrService.selectSearchAttrIds(attrIds);
        Set<Long> idSet=new HashSet<>(searchAttrIds);


        //比较baseAttrs和idSet的attrId,相同则返回true，并过滤出来
        List<SkuEsModel.Attrs> attrsList = baseAttrs.stream().filter(item -> {
            return idSet.contains(item.getAttrId());
        }).map(item -> {
            SkuEsModel.Attrs attrs1 = new SkuEsModel.Attrs();
            //把过滤出来的attrId复制到枚举类SkuEsModel.Attrs里面
            //copyProperties简单来说就是只拷贝2个对象中存在相同字段的值，其他不相同的值不拷贝，这就是浅拷贝
            BeanUtils.copyProperties(item, attrs1);
            return attrs1;
        }).collect(Collectors.toList());


        //hasStock,hotScore
        //TODO 1.发送远程调用，库存系统查询是否有库存
        Map<Long, Boolean> stockMap=null;
        try {
            R r = wareFeignService.getSkuHasStock(skuIdList);
            System.out.println(r.toString());
            //这里由于超时，stockmap的值一直没改变还是null
            TypeReference<List<SkuHasStockVo>> typeReference = new TypeReference<List<SkuHasStockVo>>(){};

            stockMap = r.getData(typeReference).stream().collect(Collectors.toMap(SkuHasStockVo::getSkuId, SkuHasStockVo::getHasStock));
        }catch (Exception e){
            log.error("库存服务查询出现异常:原因{}",e);

        }


        //2.封装每个sku的信息
        Map<Long, Boolean> finalStockMap = stockMap;
        List<SkuEsModel> upProducts = skuInfoEntities.stream().map(sku -> {
            //组装需要的数据
            SkuEsModel esModel = new SkuEsModel();
            BeanUtils.copyProperties(sku, esModel);
            esModel.setSkuPrice(sku.getPrice());
            esModel.setSkuImg(sku.getSkuDefaultImg());

            //hasStock,hotScore
            //设置库存信息
            if (finalStockMap ==null)
            {
                esModel.setHasStock(true);
            }else {
                esModel.setHasStock(finalStockMap.get(sku.getSkuId()));
            }


            //TODO 2.热度评分。暂时没想好默认为0
            esModel.setHotScore(0L);

            //TODO 3.查询品牌和分类的名字
            BrandEntity brandEntity = brandService.getById(esModel.getBrandId());
            esModel.setBrandName(brandEntity.getName());
            esModel.setBrandImg(brandEntity.getLogo());
            CategoryEntity categoryEntity = categoryService.getById(esModel.getCatalogId());
            esModel.setCatalogName(categoryEntity.getName());

            //设置检索属性
            esModel.setAttrs(attrsList);





            return esModel;
        }).collect(Collectors.toList());

        //TODO 5. 将数据发给ElasticSearch进行保存  grainmall-search
        R r = searchFeignService.productStatusUP(upProducts);
        if(r.getCode()==0){
            //远程调用成功
            //TODO 6.修改当前spu的状态
            baseMapper.updateSpuStatus(spuId, ProductConstant.StatusEnum.SPU_UP.getCode());

        }else
        {
            //远程调用失败
            //TODO 7.重复调用？接口幂等性;重试机制
            /*
            *   Feign调用流程:
            *   1.构造请求数据，将对象转为json:   SynchronousMethodHandler.class-->invoke-->RequestTemplate template = buildTemplateFromArgs.create(argv);
            *
            *   2.发送请求进行执行(执行成功会解码响应数据) :    executeAndDecode(template, options);
            *
            *   3.执行请求会有重试机制
            *       while(true)
            *       {
            *           executeAndDecode(template)
             *      }catch(){
             *          try{retryer.continueOrPropagate(e);}catch(){ throw ex;}
             *          continue;
             *      }
            *
            *
            * */

        }


    }

    @Override
    public SpuInfoEntity getSpuInfoBySkuId(Long skuId) {

        SkuInfoEntity skuInfoEntity = skuInfoService.getById(skuId);
        Long spuId = skuInfoEntity.getSpuId();

        SpuInfoEntity spuInfoEntity = getById(spuId);

        return spuInfoEntity;
    }


}