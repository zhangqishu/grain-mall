package com.zqs.grainmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zqs.common.utils.PageUtils;
import com.zqs.grainmall.product.entity.ProductAttrValueEntity;

import java.util.List;
import java.util.Map;

/**
 * spu属性值
 *
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 15:54:10
 */
public interface ProductAttrValueService extends IService<ProductAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveProductAttr(List<ProductAttrValueEntity> collect);

    /**
     * 根据spuId获取  pms_product_attr_value这张表的所有数据
     * */
    List<ProductAttrValueEntity> baseAttrlistforspu(Long spuId);

    void updateSpuAtt(Long spuId, List<ProductAttrValueEntity> entities);

}

