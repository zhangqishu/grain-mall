package com.zqs.grainmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zqs.common.utils.PageUtils;
import com.zqs.grainmall.product.entity.AttrGroupEntity;
import com.zqs.grainmall.product.vo.AttrGroupWithAttrsVo;
import com.zqs.grainmall.product.vo.SkuItemVo;
import com.zqs.grainmall.product.vo.SpuItemAttGroupVo;

import java.util.List;
import java.util.Map;

/**
 * 属性分组
 *
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 15:54:10
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage(Map<String, Object> params, Long catelogId);

    List<AttrGroupWithAttrsVo> getAttrGroupWithAttrsByCatelogId(Long catelogId);


    List<SpuItemAttGroupVo> getAttrGroupWithAttrsBySpuId(Long spuId, Long catalogId);

}

