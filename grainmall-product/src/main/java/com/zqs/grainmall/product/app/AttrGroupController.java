package com.zqs.grainmall.product.app;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


import com.zqs.grainmall.product.entity.AttrEntity;
import com.zqs.grainmall.product.service.AttrAttrgroupRelationService;
import com.zqs.grainmall.product.service.AttrService;
import com.zqs.grainmall.product.service.CategoryService;
import com.zqs.grainmall.product.vo.AttrGroupRalationVo;
import com.zqs.grainmall.product.vo.AttrGroupWithAttrsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.zqs.grainmall.product.entity.AttrGroupEntity;
import com.zqs.grainmall.product.service.AttrGroupService;
import com.zqs.common.utils.PageUtils;
import com.zqs.common.utils.R;



/**
 * 属性分组
 *
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-22 08:59:38
 */
@RestController
@RequestMapping("product/attrgroup")
public class AttrGroupController {
    @Autowired
    private AttrGroupService attrGroupService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private AttrService attrService;

    @Autowired
    private AttrAttrgroupRelationService relationService;

    //product/attrgroup/attr/relation
    @PostMapping("/attr/relation")
    public R addRelation(@RequestBody List<AttrGroupRalationVo> vos){

        relationService.saveBatch(vos);

        return R.ok();

    }

    //product/attrgroup/{catelogId}/withattr
    @GetMapping("{catelogId}/withattr")
    public R getAttrGroupWithAttrs(@PathVariable("catelogId") Long catelogId){
        //1.查出当前分类下的所有属性分组
        //2.查出每个属性分组的所有属性
        List<AttrGroupWithAttrsVo> vos=attrGroupService.getAttrGroupWithAttrsByCatelogId(catelogId);

        return R.ok().put("data",vos);
    }

    //product/attrgroup/{attrgroupId}/attr/relation
    @GetMapping("/{attrgroupId}/attr/relation")
    public R attrRelation(@PathVariable("attrgroupId") Long attrgroupId){

       List<AttrEntity> entities= attrService.getRelationAttr(attrgroupId);
        return R.ok().put("data",entities);
    }

    //product/attrgroup/{attrgroupId}/noattr/relation
    @GetMapping("/{attrgroupId}/noattr/relation")
    public R attrNoRelation(@PathVariable("attrgroupId") Long attrgroupId,
                            @RequestParam Map<String,Object> params){

        PageUtils page= attrService.getNoRelationAttr(params,attrgroupId);
        return R.ok().put("page",page);
    }

    /**
     * 列表
     */
    @RequestMapping("/list/{catelogId}")
    //@RequiresPermissions("product:attrgroup:list")
    public R list(@RequestParam Map<String, Object> params,
                  @PathVariable("catelogId") Long catelogId){
       // PageUtils page = attrGroupService.queryPage(params);

        PageUtils page=attrGroupService.queryPage(params,catelogId);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{attrGroupId}")
    //@RequiresPermissions("product:attrgroup:info")
    public R info(@PathVariable("attrGroupId") Long attrGroupId){
		AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);

        Long catelogId = attrGroup.getCatelogId();
        //目的就是：根据为传过来的catelogId 给 category表中的catid来查它的父节点
        Long [] path= categoryService.findCatelogPath(catelogId);


        attrGroup.setCatelogPath(path);

        return R.ok().put("attrGroup", attrGroup);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
   // @RequiresPermissions("product:attrgroup:save")
    public R save(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.save(attrGroup);

        return R.ok();
    }


    /**
     *
     * product/attrgroup/attr/relation/delete
     * */

    @PostMapping("/attr/relation/delete")
    public R deleteRelation(@RequestBody AttrGroupRalationVo[] vos){
        attrService.deleteRelation(vos);
        return R.ok();
    }




    /**
     * 修改
     */
    @RequestMapping("/update")
   // @RequiresPermissions("product:attrgroup:update")
    public R update(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.updateById(attrGroup);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
   // @RequiresPermissions("product:attrgroup:delete")
    public R delete(@RequestBody Long[] attrGroupIds){
		attrGroupService.removeByIds(Arrays.asList(attrGroupIds));

        return R.ok();
    }

}
