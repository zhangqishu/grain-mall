package com.zqs.grainmall.product.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @program: grain-mall
 * @description:  这个类的作用就是把线程池的配置变为可配置的，我们就可以在配置文件中改
 * @author: Mr.Zhang
 * @create: 2020-11-16 17:14
 **/

@ConfigurationProperties(prefix = "grainmall.thread")
@Component
@Data
public class ThreadPoolConfigProperties {
    private Integer coreSize;
    private Integer maxSize;
    private Integer keepAliveTime;
}
