package com.zqs.grainmall.product.app;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


import com.zqs.grainmall.product.entity.ProductAttrValueEntity;
import com.zqs.grainmall.product.service.ProductAttrValueService;
import com.zqs.grainmall.product.vo.AttrRespVo;
import com.zqs.grainmall.product.vo.AttrVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.zqs.grainmall.product.service.AttrService;
import com.zqs.common.utils.PageUtils;
import com.zqs.common.utils.R;

import javax.annotation.Resource;


/**
 * 商品属性
 *
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-22 08:59:38
 */
@RestController
@RequestMapping("product/attr")
public class AttrController {
    @Resource
    private AttrService attrService;

    @Autowired
    ProductAttrValueService productAttrValueService;

    //product/attr/sale/list

    @GetMapping("/base/listforspu/{spuId}")
    public R baseAttrList(@PathVariable("spuId") Long spuId){

       List<ProductAttrValueEntity> entities=productAttrValueService.baseAttrlistforspu(spuId);


        return R.ok().put("data",entities);

    }

    //product/attr/base/list/{catelogId}
    @GetMapping("/{attrType}/list/{catelogId}")
    public R baseAttrList(@RequestParam Map<String,Object> params,
                          @PathVariable("catelogId") Long catelogId,
                          @PathVariable("attrType") String type) {
      PageUtils page= attrService.queryBaseAttrPage(params,catelogId,type);
        return R.ok().put("page",page);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("product:attr:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = attrService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{attrId}")
    //@RequiresPermissions("product:attr:info")
    public R info(@PathVariable("attrId") Long attrId){
		//AttrEntity attr = attrService.getById(attrId);
       AttrRespVo respVo=attrService.getAttrInfo(attrId);

        return R.ok().put("attr",respVo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
   // @RequiresPermissions("product:attr:save")
    public R save(@RequestBody AttrVo attr){
		attrService.saveAttr(attr);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
   // @RequiresPermissions("product:attr:update")
    public R update(@RequestBody AttrVo attr){
		attrService.updateAttr(attr);

        return R.ok();
    }

    /**
     *  获取规格后修改
     * */
    @PostMapping("/update/{spuId}")
    public R updateSpuAttr(@PathVariable("spuId") Long spuId,
                           @RequestBody List<ProductAttrValueEntity> entities)
    {
       productAttrValueService.updateSpuAtt(spuId,entities);


        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
   // @RequiresPermissions("product:attr:delete")
    public R delete(@RequestBody Long[] attrIds){
		attrService.removeByIds(Arrays.asList(attrIds));

        return R.ok();
    }

}
