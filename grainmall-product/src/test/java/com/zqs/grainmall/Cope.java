package com.zqs.grainmall;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.rowset.spi.SyncResolver;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-02 22:04
 **/

@Slf4j
@SpringBootTest
public class Cope {
    @Test
    public void test1(){

        @Data
        class Account{
            private String username;
            private String password;
            private String dogName;
        }
        @Data
        class Dog{
            private String dogName;
            private String doggender;
            private String username;
        }

       Account account=new Account();
        account.setUsername("张三");
        account.setPassword("111");
        account.setDogName("改变了");

        Dog dog=new Dog();
        dog.setDogName("李四");
        dog.setDoggender("222");
        dog.setUsername("assa");
//        BeanUtils.copyProperties(account,dog);
//        System.out.println(account);
//        System.out.println(dog);

        List<Long> list=new ArrayList<>();
        list.add(1L);
        list.add(2L);

        List<Integer> list2=new ArrayList<>();
        list.add(1L);
        list.add(2L);
        List<Dog> collect = list.stream().map(x -> {
            Dog dog1 = new Dog();
            return dog1;
        }).collect(Collectors.toList());

        System.out.println(collect);
    }

}
