package com.zqs.grainmall;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zqs.grainmall.product.dao.AttrGroupDao;
import com.zqs.grainmall.product.dao.SkuSaleAttrValueDao;
import com.zqs.grainmall.product.entity.BrandEntity;
import com.zqs.grainmall.product.entity.CategoryEntity;
import com.zqs.grainmall.product.service.BrandService;
import com.zqs.grainmall.product.service.CategoryService;
import com.zqs.grainmall.product.vo.SkuItemSaleAttrVo;
import com.zqs.grainmall.product.vo.SkuItemVo;
import com.zqs.grainmall.product.vo.SpuItemAttGroupVo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

/**
 * 阿里云OSS服务:
 *  1.引入对象存储的oss-starter
 *  2.配置key,endpoint相关信息即可
 *  3.使用OSSClient对象存储
 *
 * */
@Slf4j
@SpringBootTest
class GrainmallProductApplicationTests {

    @Autowired
    BrandService brandService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    AttrGroupDao attrGroupDao;

    @Autowired
    SkuSaleAttrValueDao skuSaleAttrValueDao;

    @Test
    public void test2(){
        List<SkuItemSaleAttrVo> saleAttrsBySpuId = skuSaleAttrValueDao.getSaleAttrsBySpuId(20L);
        System.out.println(saleAttrsBySpuId);
    }

    @Test
    public void test(){
        List<SpuItemAttGroupVo> groupVos = attrGroupDao.getAttrGroupWithAttrsBySpuId(17L, 225L);
        System.out.println(groupVos);

    }

  //  @Autowired
//    OSSClient ossClient;

    @Autowired
    StringRedisTemplate stringRedisTemplate;


    @Autowired
    RedissonClient redissonClient;

    @Test
    public void redisson()
    {
        System.out.println(redissonClient);


    }

    @Test
    public void teststringredisTemplate(){
        //hello world
        ValueOperations<String, String> forValue = stringRedisTemplate.opsForValue();

        //保存
        forValue.set("hello","word_"+UUID.randomUUID().toString());

        //查询
        String s = forValue.get("hello");
        System.out.println(s);

    }




    @Test
    public void testUpload() throws FileNotFoundException {
        // 上传文件流。
//        InputStream inputStream = new FileInputStream("/Users/zhangqishu/Downloads/3175DC63DC8F767C43B0409D7E5FFD7D.png");
//
//        ossClient.putObject("grainmall-zqs", "zqs1.png", inputStream);
//
//        // 关闭OSSClient。
//        ossClient.shutdown();
//
//        System.out.println("上传成功");
    }

    @Test
    void contextLoads() {
        //测试插入
        BrandEntity entity = new BrandEntity();
        entity.setName("华为");
        brandService.save(entity);
        System.out.println("保存成功......");

    }

    @Test
    void update(){
        //测试更新
        BrandEntity entity=new BrandEntity();
        entity.setBrandId(1L);
        entity.setDescript("华为");
        brandService.updateById(entity);
        System.out.println("修改成功");
    }

    @Test
    void page(){
        List<BrandEntity> list = brandService.list(new QueryWrapper<BrandEntity>().eq("brand_id", 1L));
        list.forEach(System.out::println);
    }

    @Test
    void filter(){
        Integer []a={1,25,34,41,11,22,33};
        Arrays.stream(a).filter(x->x==1).forEach(System.out::println);

    }

    @Test
    void  testMap(){
        Map<String,String> map=new HashMap<>();
        map.put("key1","aaaa");
        map.put("key2","bbbbb");
        map.put("key3","cccc");
        System.out.println(map.get("key1"));
    }

    @Test
    void testFindpath(){
       Long[] catelogPath=categoryService.findCatelogPath(225L);
       log.info("完整路径:{}",Arrays.asList(catelogPath));

    }

    /**
     *  java8小练习
     * */
    @Test
    void test1(){
        for(int i = 2; i < 5; i++) {
            System.out.print(i + "...");
        }
        System.out.println("==============");
        IntStream.range(2,5).forEach(i-> System.out.print(i+"...."));

        System.out.println("==============");
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        IntStream.range(0, 5)
                .forEach(i -> executorService.submit(() -> System.out.println("Running task " + i)));

        System.out.println("==============");

        IntStream.rangeClosed(0,5).forEach(i-> System.out.print(i+"...."));

        System.out.println("==============");
        System.out.println(IntStream.iterate(1, e -> e + 1)
                .limit(10).sum());

    }

}
