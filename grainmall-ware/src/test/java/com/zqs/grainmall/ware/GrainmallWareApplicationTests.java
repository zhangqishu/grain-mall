package com.zqs.grainmall.ware;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
class GrainmallWareApplicationTests {

    @Test
    void contextLoads() {
        ArrayList<String> list = new ArrayList<>();
        list.add("添加第一个元素");
        List<String> list1 = Collections.unmodifiableList(list);
        list1.add("添加第二个元素");
        System.out.println(list.size());
    }

}
