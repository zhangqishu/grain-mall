package com.zqs.grainmall.ware.dao;

import com.zqs.grainmall.ware.entity.PurchaseDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 20:11:31
 */
@Mapper
public interface PurchaseDetailDao extends BaseMapper<PurchaseDetailEntity> {
	
}
