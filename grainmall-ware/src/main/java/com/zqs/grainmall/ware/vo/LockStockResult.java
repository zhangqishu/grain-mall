package com.zqs.grainmall.ware.vo;

import lombok.Data;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-24 15:54
 **/
@Data
public class LockStockResult {
    private Long skuId;
    private Integer num;  //锁了多少件
    private Boolean locked;  //锁定成功了没有
}
