package com.zqs.grainmall.ware.feign;


import com.zqs.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("grainmall-product")
public interface ProductFeignService {

    /**
     *   1./product/skuinfo/info/{skuId}
     *    ---  直接调用对应服务
     *   2.api/product/skuinfo/info/{skuId}
     *    ---  让所有请求过网关
     *
     *
     *
     *
     * */
    @RequestMapping("/product/skuinfo/info/{skuId}")
    public R info(@PathVariable("skuId") Long skuId);
}
