package com.zqs.grainmall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zqs.common.to.mq.OrderTo;
import com.zqs.common.to.mq.StockLockedTo;
import com.zqs.common.utils.PageUtils;
import com.zqs.grainmall.ware.entity.WareSkuEntity;
import com.zqs.grainmall.ware.vo.LockStockResult;
import com.zqs.grainmall.ware.vo.SkuHasStockVo;
import com.zqs.grainmall.ware.vo.WareSkuLockVO;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 20:11:31
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);


    void addStock(Long skuId, Long wareId, Integer skuNum);

    List<SkuHasStockVo> getSkuHasStock(List<Long> skuIds);

    Boolean orderLockStock(WareSkuLockVO vo);

    void unlockStock(StockLockedTo to);

    void unlockStock(OrderTo orderTo);

}

