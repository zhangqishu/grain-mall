package com.zqs.grainmall.ware.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-10-31 14:53
 **/
@Data
public class PurchaseDoneVo {
    @NotNull
    private Long id;  //采购单id
    private List<PurchaseItemDoneVo> items;
}
