package com.zqs.grainmall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-24 15:48
 **/
@Data
public class WareSkuLockVO {
    private String orderSn;  //订单号
    private List<OrderItemVo> locks; //需要锁住的所有库存信息
}
