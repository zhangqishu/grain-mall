package com.zqs.grainmall.ware.dao;

import com.zqs.grainmall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 20:11:31
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
