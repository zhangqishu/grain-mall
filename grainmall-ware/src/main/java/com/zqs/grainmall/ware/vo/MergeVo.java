package com.zqs.grainmall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-10-31 09:54
 **/

@Data
public class MergeVo {

    //purchaseId: 1, //整单id
    //items:[1,2,3,4] //合并项集合

    private Long purchaseId;
    private List<Long> items;


}
