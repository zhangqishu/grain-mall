package com.zqs.grainmall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zqs.common.utils.PageUtils;
import com.zqs.grainmall.ware.entity.WareInfoEntity;
import com.zqs.grainmall.ware.vo.FareVo;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 仓库信息
 *
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 20:11:31
 */
public interface WareInfoService extends IService<WareInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);


    /**
     * 根据用户的收获地址计算运费
     * */
    FareVo getFare(Long addrId);
}

