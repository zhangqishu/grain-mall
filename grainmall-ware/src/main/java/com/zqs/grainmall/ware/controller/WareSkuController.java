package com.zqs.grainmall.ware.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


import com.zqs.common.exception.BizCodeEnums;
import com.zqs.common.exception.NoStockException;
import com.zqs.grainmall.ware.vo.SkuHasStockVo;
import com.zqs.grainmall.ware.vo.WareSkuLockVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.zqs.grainmall.ware.entity.WareSkuEntity;
import com.zqs.grainmall.ware.service.WareSkuService;
import com.zqs.common.utils.PageUtils;
import com.zqs.common.utils.R;



/**
 * 商品库存
 *
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 20:11:31
 */
@RestController
@RequestMapping("ware/waresku")
public class WareSkuController {
    @Autowired
    private WareSkuService wareSkuService;

    /**
     * 锁库存
     * */
    @PostMapping("/lock/order")
    public R  orderLockStock(@RequestBody WareSkuLockVO vo){

        try {
            Boolean stock=  wareSkuService.orderLockStock(vo);
            return R.ok();
        } catch (NoStockException e) {
            return R.error(BizCodeEnums.NO_STOCK_EXCEPTION.getCode(),BizCodeEnums.NO_STOCK_EXCEPTION.getMsg());
        }


    }

    //查询sku是否有库存
    @PostMapping("/hasstock")
    public R getSkuHasStock(@RequestBody List<Long> skuIds){

        //sku_id,stock
        List<SkuHasStockVo> vos=wareSkuService.getSkuHasStock(skuIds);


        return R.ok().setData(vos);

    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("ware:waresku:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = wareSkuService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("ware:waresku:info")
    public R info(@PathVariable("id") Long id){
		WareSkuEntity wareSku = wareSkuService.getById(id);

        return R.ok().put("wareSku", wareSku);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
   // @RequiresPermissions("ware:waresku:save")
    public R save(@RequestBody WareSkuEntity wareSku){
		wareSkuService.save(wareSku);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
   // @RequiresPermissions("ware:waresku:update")
    public R update(@RequestBody WareSkuEntity wareSku){
		wareSkuService.updateById(wareSku);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
   // @RequiresPermissions("ware:waresku:delete")
    public R delete(@RequestBody Long[] ids){
		wareSkuService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
