package com.zqs.grainmall.ware.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-22 21:05
 **/

@Data
public class FareVo {
    private  MemberAddressVo address;
    private BigDecimal fare;

}
