package com.zqs.grainmall.auth.config;

import lombok.Data;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @program: grain-mall
 * @description: 视图映射
 * @author: Mr.Zhang
 * @create: 2020-11-16 21:10
 **/

@Configuration
public class GrainmallWebConfig implements WebMvcConfigurer {
    /**
     *原来我们是这样写的:
     *   @GetMapping("/login.html")
     *     public String loginPage(){
     *
     *
     *         return "login";
     *     }
     *
     * */
    //使用Springboot简化这种操作的方式:
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login.html").setViewName("login");
        registry.addViewController("/reg.html").setViewName("reg");
    }
}
