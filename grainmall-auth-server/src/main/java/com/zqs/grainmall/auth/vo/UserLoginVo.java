package com.zqs.grainmall.auth.vo;

import lombok.Data;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-17 16:34
 **/

@Data
public class UserLoginVo {
    private String loginacct;
    private String password;
}
