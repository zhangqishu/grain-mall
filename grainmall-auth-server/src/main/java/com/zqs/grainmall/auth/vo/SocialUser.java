package com.zqs.grainmall.auth.vo;

import lombok.Data;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-17 22:44
 **/
@Data
public class SocialUser {


    private String access_token;

    private String remind_in;

    private long expires_in;

    private String uid;

    private String isRealName;


}
