package com.zqs.grainmall.member;

import com.zqs.grainmall.member.feign.CouponFeignService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class GrainmallMemberApplicationTests {

    @Autowired
    CouponFeignService couponFeignService;

    @Test
    void contextLoads() {
        Object o = couponFeignService.membercoupons().get("coupons");
        System.out.println(o);

    }

}
