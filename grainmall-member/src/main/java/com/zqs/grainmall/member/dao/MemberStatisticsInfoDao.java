package com.zqs.grainmall.member.dao;

import com.zqs.grainmall.member.entity.MemberStatisticsInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员统计信息
 * 
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 18:21:15
 */
@Mapper
public interface MemberStatisticsInfoDao extends BaseMapper<MemberStatisticsInfoEntity> {
	
}
