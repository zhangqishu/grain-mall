package com.zqs.grainmall.member.controller;

import java.util.Arrays;
import java.util.Map;


import com.zqs.common.exception.BizCodeEnums;
import com.zqs.grainmall.member.exception.PhoneExistException;
import com.zqs.grainmall.member.exception.UsernameExistException;
import com.zqs.grainmall.member.feign.CouponFeignService;
import com.zqs.grainmall.member.vo.MemberLoginVo;
import com.zqs.grainmall.member.vo.MemberRegistVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.zqs.grainmall.member.entity.MemberEntity;
import com.zqs.grainmall.member.service.MemberService;
import com.zqs.common.utils.PageUtils;
import com.zqs.common.utils.R;



/**
 * 会员
 *
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 18:21:15
 */
@RestController
@RequestMapping("member/member")
public class MemberController {
    @Autowired
    private MemberService memberService;

    @Autowired
    CouponFeignService couponFeignService;

    @RequestMapping("/coupons")
    public R test(){
        MemberEntity entity = new MemberEntity();
        entity.setNickname("张三");
        R  membercoupons = couponFeignService.membercoupons();
        return R.ok().put("member",entity).put("coupons", membercoupons.get("coupons"));

    }

    /**
     *
     * 注册页面
     * */
    @PostMapping("/regist")
    public R regist(@RequestBody MemberRegistVo vo){
        try {
            memberService.regist(vo);

        }catch (PhoneExistException e)
        {
            return R.error(BizCodeEnums.USER_EXIST_EXCEPTION.getCode(),BizCodeEnums.USER_EXIST_EXCEPTION.getMsg());
        }catch (UsernameExistException e)
        {
            return R.error(BizCodeEnums.PHONE_EXIST_EXCEPTION.getCode(),BizCodeEnums.PHONE_EXIST_EXCEPTION.getMsg());

        }

        return R.ok();

    }


    /**
     * 登陆页面
     *
     * */
    @PostMapping("/login")
    public R login(@RequestBody MemberLoginVo vo){

       MemberEntity entity= memberService.login(vo);
       if (entity!=null)
       {
           return R.ok().setData(entity);
       }
       else {
           return R.error(BizCodeEnums.LOGINACCT_PASSWORD_INVAILD_EXCEPTION.getCode(),BizCodeEnums.LOGINACCT_PASSWORD_INVAILD_EXCEPTION.getMsg());
       }


    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("member:member:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = memberService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("member:member:info")
    public R info(@PathVariable("id") Long id){
		MemberEntity member = memberService.getById(id);

        return R.ok().put("member", member);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
   // @RequiresPermissions("member:member:save")
    public R save(@RequestBody MemberEntity member){
		memberService.save(member);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
   // @RequiresPermissions("member:member:update")
    public R update(@RequestBody MemberEntity member){
		memberService.updateById(member);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
   // @RequiresPermissions("member:member:delete")
    public R delete(@RequestBody Long[] ids){
		memberService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
