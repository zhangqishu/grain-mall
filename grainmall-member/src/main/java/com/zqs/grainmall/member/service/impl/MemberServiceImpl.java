package com.zqs.grainmall.member.service.impl;

import com.zqs.grainmall.member.dao.MemberLevelDao;
import com.zqs.grainmall.member.entity.MemberLevelEntity;
import com.zqs.grainmall.member.exception.PhoneExistException;
import com.zqs.grainmall.member.exception.UsernameExistException;
import com.zqs.grainmall.member.vo.MemberLoginVo;
import com.zqs.grainmall.member.vo.MemberRegistVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zqs.common.utils.PageUtils;
import com.zqs.common.utils.Query;

import com.zqs.grainmall.member.dao.MemberDao;
import com.zqs.grainmall.member.entity.MemberEntity;
import com.zqs.grainmall.member.service.MemberService;


@Service("memberService")
public class MemberServiceImpl extends ServiceImpl<MemberDao, MemberEntity> implements MemberService {

    @Autowired
    MemberLevelDao memberLevelDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MemberEntity> page = this.page(
                new Query<MemberEntity>().getPage(params),
                new QueryWrapper<MemberEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void regist(MemberRegistVo vo) {
        MemberDao memberDao=this.baseMapper;
        MemberEntity entity = new MemberEntity();

        MemberLevelEntity levelEntity=memberDao.getDefaultLevel();
        //设置默认等级
        entity.setLevelId(levelEntity.getId());

        //检查用户名和手机号是否唯一,为了让controller能感知异常，异常机制
        checkPhoneUnique(vo.getPhone());
        checkUsernameUnique(vo.getUserName());

        entity.setNickname(vo.getUserName());
        entity.setUsername(vo.getUserName());

        entity.setMobile(vo.getPhone());
        entity.setUsername(vo.getUserName());

        entity.setNickname(vo.getUserName());

        //密码要进行加密操作，md5:不可逆加密 使用Spring提供的BCryptPasswordEncoder来加密
        BCryptPasswordEncoder passwordEncoder=new BCryptPasswordEncoder();
        String encode = passwordEncoder.encode(vo.getPassword());
        entity.setPassword(encode);
        entity.setGender(0);
        entity.setCreateTime(new Date());


        //其他默认信息


        //保存
        memberDao.insert(entity);

    }

    @Override
    public void checkPhoneUnique(String phone) throws PhoneExistException {
        Integer mobile = this.baseMapper.selectCount(new QueryWrapper<MemberEntity>().eq("mobile", phone));
        if (mobile>0)
        {
            throw new PhoneExistException();
        }

    }

    @Override
    public void checkUsernameUnique(String username) throws UsernameExistException {
        Integer name = this.baseMapper.selectCount(new QueryWrapper<MemberEntity>().eq("username", username));
        if (name>0)
        {
            throw new UsernameExistException();

        }

    }

    @Override
    public MemberEntity login(MemberLoginVo vo) {
        String loginacct = vo.getLoginacct();
        String password = vo.getPassword();

        //1.去数据库查询
        MemberEntity entity = this.baseMapper.selectOne(new QueryWrapper<MemberEntity>()
                .eq("username", vo.getLoginacct())
                .or()
                .eq("mobile", loginacct));
        if (entity==null)
        {
            //登陆失败
            return null;
        }
        else {
            //1.获取数据库的password
            String passwordDb = entity.getPassword();
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            //判断传过来的密码和数据库的密码是否匹配
            boolean matches = encoder.matches(password, passwordDb);
            if (matches)
            {
                return entity;
            }
            else {
                return null;
            }
        }
    }

}