package com.zqs.grainmall.member.exception;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-17 13:39
 **/
public class UsernameExistException extends RuntimeException {
    public UsernameExistException(){
        super("用户名已存在");
    }
}
