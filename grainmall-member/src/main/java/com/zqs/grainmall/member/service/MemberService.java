package com.zqs.grainmall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zqs.common.utils.PageUtils;
import com.zqs.grainmall.member.entity.MemberEntity;
import com.zqs.grainmall.member.exception.PhoneExistException;
import com.zqs.grainmall.member.exception.UsernameExistException;
import com.zqs.grainmall.member.vo.MemberLoginVo;
import com.zqs.grainmall.member.vo.MemberRegistVo;

import java.util.Map;

/**
 * 会员
 *
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 18:21:15
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void regist(MemberRegistVo vo);

    void checkPhoneUnique(String phone) throws PhoneExistException;

    void checkUsernameUnique(String email) throws UsernameExistException;

    MemberEntity login(MemberLoginVo vo);

}

