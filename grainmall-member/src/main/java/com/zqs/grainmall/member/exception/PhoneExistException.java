package com.zqs.grainmall.member.exception;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-17 13:39
 **/

public class PhoneExistException extends RuntimeException{

    public PhoneExistException(){
        super("手机号已存在");
    }

}
