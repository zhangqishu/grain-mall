package com.zqs.grainmall.member.vo;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-17 13:22
 **/
@Data
public class MemberRegistVo {
    private String userName;
    private String password;
    private String phone;
}
