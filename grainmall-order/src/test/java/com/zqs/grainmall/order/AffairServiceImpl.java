package com.zqs.grainmall.order;

import org.springframework.aop.framework.AopContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-25 10:33
 **/
@Service
public class AffairServiceImpl {

    @Transactional()
    public void a(){
//        b();  //a事务
//        c();   //新事务
        AffairServiceImpl affairService = (AffairServiceImpl) AopContext.currentProxy();
        affairService.b();
        affairService.c();

        int i=10/0;
    }
    @Transactional(propagation = Propagation.REQUIRED)
    public void b(){

    }
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void c(){

    }
}
