package com.zqs.grainmall.order;

import com.zqs.grainmall.order.entity.OrderReturnReasonEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@Slf4j
@SpringBootTest
class GrainmallOrderApplicationTests {

    @Autowired
    AmqpAdmin amqpAdmin;

    @Autowired
    RabbitTemplate rabbitTemplate;



    @Test
    public void test3() throws ExecutionException, InterruptedException {

        Integer[] integer = {1, 2, 4, 6, 7, 8};
        System.out.println(integer[1]+2);
        List<Integer> list=null;
        for (int i = 0; i <integer.length-1; i++) {
            if (integer[i]+2==integer[i+1])
            {
                list= Arrays.asList(integer[i], integer[i -1]);
            }

        }
        System.out.println(list);



//        CompletableFuture.runAsync(() ->
//        {
//            Arrays.stream(integer).filter(item ->
//            {
//                return item > 6;
//            }).forEach(System.out::println);
//        });
//
//        CompletableFuture.runAsync(() -> {
//            Arrays.stream(integer).filter(item -> {
//                return item < 3;
//            }).forEach(System.out::println);
//        });
    }


    class Temp{
        Integer total=0;
    }

    @Test
    public void test10(){
        Temp temp = new Temp();
        for (int i = 0; i < 3000; i++) {
            new Thread(()->{synchronized (temp){temp.total++;}}).start();
        }
        System.out.println(temp.total);
    }

    /**
     * 1.如果创建exchange[hello-java-exchange],Queue,Binding
     * 2.如何收发消息
     *
     * */

    @Test
    public void creatMessage(){
        for (int i = 0; i < 10; i++) {
            OrderReturnReasonEntity orderReturnReasonEntity = new OrderReturnReasonEntity();
            orderReturnReasonEntity.setId(1L);
            orderReturnReasonEntity.setName("顾沉舟"+i);
            orderReturnReasonEntity.setSort(1);
            orderReturnReasonEntity.setStatus(2);
            //1.发送消息  如果发送的消息是个对象，我们会使用序列化机制，将对象写出去，对象必须实现Serializable
            //2.发送的消息转化为JSON
            rabbitTemplate.convertAndSend("hello-java-exchange", "hello.java",
                    orderReturnReasonEntity,
                    new CorrelationData(UUID.randomUUID().toString()));
            log.info("消息发送成功");
        }
    }


    @Test
    public void createExchange(){

        //new一个Direct类型的交换机
        DirectExchange directExchange = new DirectExchange("hello-java-exchange",true,false);
        //声明这个交换机
        amqpAdmin.declareExchange(directExchange);
        log.info("交换机创建完成[{}]","hello-java-exchange");

    }

    @Test
    public void createQueue(){
        Queue queue = new Queue("hello-java-queue",true,false,false);
        amqpAdmin.declareQueue(queue);
        log.info("队列创建完成[{}]","hello-java-queue");

    }

    @Test
    public void creatBanding(){
        //String destination,目的地
        // DestinationType  目的地类型
        // destinationType, 参数
        // String exchange,交互机
        // String routingKey 路由键
        // Map<String, Object> arguments 自定义参数
        //将exchange指定的交换机和destination目的地进行绑定，使用routingKey作为指定的路由键
        Binding binding = new Binding("hello-java-queue",
                Binding.DestinationType.QUEUE,
                "hello-java-exchange",
                "hello.java",null);
        amqpAdmin.declareBinding(binding);
        log.info("Binding创建完成[{}]","hello-java-binding");
    }

}
