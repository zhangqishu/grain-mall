package com.zqs.grainmall.order;

import com.alibaba.cloud.seata.GlobalTransactionAutoConfiguration;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;


/**
 *
 * Springboot整合rabbitmq
 *      1.引入amqp场景
 *         RabbitAutoConfiguration
 *      2.容器中自动配置了
 *         RabbitTemplate(收发消息)、AmqpAdmin(创建交换机，绑定关系，队列等)、CachingConnectionFactory、RabbitMessagingTemplate
 *         所有的配置属性都是在
 *              @ConfigurationProperties (prefix = "spring.rabbitmq")
 *      3.@EnableRabbit
 *      4.给配置文件中配置  spring.rabbitmq信息
 *      5.发消息RabbitTemplate.convertAndSend等等
 *      6.监听消息
 *          @RabbitListener: 类+方法上(监听哪些队列)
 *          @RabbitHandler: 标在方法上(重载区分不同的消息)
 *
 *
 *  本地事务失效问题
 *  同一个对象内事务方法互调默认失效，原因: 绕过了代理对象，事务使用代理对象来控制
 *  使用代理对象来调用事务方法
 *      1.引入aop-starter  引入了aspectj
 *      2. @EnableAspectJAutoProxy：开启aspectj动态代理功能，以后所有的动态代理都是aspectj创建的(即使没有接口也可以创建动态代理)
 *         对外暴露代理对象
 *      3.本类用代理对象互调
 *
 * Seata控制分布式事务
 *   1.每一个微服务必须先创建一个undo_log表
 *   2.安装事务协调器: seata-server:  https://github.com/seata/seata/releases
 *   3.整合
 *     1.导入依赖 spring-cloud-starter-alibaba-seata   seata:seata-all:0.7.1
 *     2.启动seata服务器 解压并启动seata-server;
 *        registry.conf: 注册中心的配置  type=nacos
 *     3.所有想要用到分布式事务的微服务必须使用seata DataSourceProxy代理自己的数据源,看我的配置类
 *     4.每个微服务，都必须导入  registry.conf
 *                            file.conf     vgroup_mapping.{application.name}-fescar-service-group = "default"
 *     5.启动测试分布式事务
 *     6.给分布式事务的大入口标注  @GlobalTransactional  全局事务
 *     7.每一个远程的小事务用@Transactional
 *
 * */

@EnableAspectJAutoProxy
@EnableFeignClients
@EnableRedisHttpSession
@EnableRabbit
@SpringBootApplication(exclude = GlobalTransactionAutoConfiguration.class)
@EnableDiscoveryClient
public class GrainmallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(GrainmallOrderApplication.class, args);
    }

}
