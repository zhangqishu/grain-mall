package com.zqs.grainmall.order.web;

import com.zqs.grainmall.order.service.OrderService;
import com.zqs.grainmall.order.vo.OrderConfirmVo;
import com.zqs.grainmall.order.vo.OrderSubmitVo;
import com.zqs.grainmall.order.vo.SubmitOrderResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.concurrent.ExecutionException;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-21 10:22
 **/

@Controller
public class OrderWebController {

    @Autowired
    OrderService orderService;

    @GetMapping("/toTrade")
    public String toTrade(Model model) throws ExecutionException, InterruptedException {

       OrderConfirmVo confirmVo=orderService.confirmOrder();
       model.addAttribute("confirmOrderData",confirmVo);

        //展示订单的数据

        return "confirm";
    }

    /**
     * 下单功能
     *
     * */
    @PostMapping("/submitOrder")
    public String submitOrder(OrderSubmitVo vo, Model model, RedirectAttributes redirectAttributes){

        //下单去创建订单，验令牌，验价格，锁库存.....
       SubmitOrderResponseVo responseVo=orderService.submitOrder(vo);

        //下单成功来到支付选择页


        System.out.println("订单提交的数据...."+vo);
        if (responseVo.getCode()==0)
        {
            //下单成功来到支付选择页
            model.addAttribute("submitOrderResp",responseVo);

            return "pay";

        }else
        {//下单失败回到订单确认页重新确认订单信息

            String msg="下单失败";
            switch (responseVo.getCode()) {
                case 1: msg+="订单信息过期，请刷新再次提交"; break;
                case 2: msg+="订单商品价格发送变化，请确认后提交"; break;
                case 3: msg+="库存锁定失败，商品库存不足"; break;
            }
            redirectAttributes.addFlashAttribute("msg",msg);
            return "redirect:http://127.0.0.1:9010/toTrade";
        }

    }
}
