package com.zqs.grainmall.order.to;

import com.zqs.grainmall.order.entity.OrderEntity;
import com.zqs.grainmall.order.entity.OrderItemEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-23 17:05
 **/

@Data
public class OrderCreateTo {

    private OrderEntity order;

    private List<OrderItemEntity> orderItems;

    private BigDecimal payPrice; //订单计算的应付价格

    private BigDecimal fare;  //运费

}
