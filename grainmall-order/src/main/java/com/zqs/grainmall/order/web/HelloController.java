package com.zqs.grainmall.order.web;

import com.rabbitmq.client.Channel;
import com.zqs.grainmall.order.entity.OrderEntity;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.UUID;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-21 09:44
 **/

@Controller
public class HelloController {

    @Autowired
    RabbitTemplate rabbitTemplate;

    @ResponseBody
    @GetMapping("/test/createOrder")
    public String createOrderTest(OrderEntity entity){
        //订单下单成功
      OrderEntity orderEntity= new OrderEntity();
      orderEntity.setOrderSn(UUID.randomUUID().toString());
      orderEntity.setModifyTime(new Date());

      //给MQ发送消息
        rabbitTemplate.convertAndSend("order-event-exchange","order.create.order",orderEntity);
      return "ok";


    }

    @GetMapping("/{page}.html")
    public String listPage(@PathVariable("page") String page){

        return page;

    }
}
