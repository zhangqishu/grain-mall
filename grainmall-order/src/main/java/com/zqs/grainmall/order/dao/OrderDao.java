package com.zqs.grainmall.order.dao;

import com.zqs.grainmall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 18:35:44
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
