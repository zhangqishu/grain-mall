package com.zqs.grainmall.order.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-23 18:00
 **/

@Data
public class FareVo {
    private MemberAddressVo address;
    private BigDecimal fare;
}
