package com.zqs.grainmall.order.config;

import com.rabbitmq.client.Channel;
import com.zqs.grainmall.order.entity.OrderEntity;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @program: grain-mall
 * @description: 配置消息的过期时间和死信队列
 * @author: Mr.Zhang
 * @create: 2020-11-26 16:55
 **/
@Configuration
public class MyMQConfig {

    //Binding,Queue,Exchange
    //RabbitMQ 只要有。@Bean声明属性发生变化也不会覆盖

    /**
     * Bean:添加修改组件
     * 容器中的Binding,Queue,Exchange都会自动创建(RabbitMQ没有都情况)
     *
     * <p>
     * 创建队列
     */
    @Bean
    public Queue orderDelayQueue() {
        /*
         * queue参数:
         *   (String name, boolean durable, boolean exclusive, boolean autoDelete,@Nullable Map<String, Object> arguments)
         * name: 队列的名字
         * durable: 是否是持久化的
         * exclusive: 是否是排他的
         * autoDelete: 是否是自动删除的
         * arguments: 创建自定义属性
         *
         * */
        Map<String, Object> arguments = new HashMap<>();
        arguments.put("x-dead-letter-exchange", "order-event-exchange");
        arguments.put("x-dead-letter-routing-key", "order.release.order");
        arguments.put("x-message-ttl", 60000);
        Queue queue = new Queue("order.delay.queue", true, false, false, arguments);

        return queue;

    }


    /**
     * 创建队列
     */
    @Bean
    public Queue orderReleaseOrderQueue() {


        Queue queue = new Queue("order.release.order.queue", true, false, false);

        return queue;
    }


    /**
     * 创建主题式路由
     */
    @Bean
    public Exchange orderEventExchange() {

        //(String name, boolean durable, boolean autoDelete, Map<String, Object> arguments)
        //
        TopicExchange topicExchange = new TopicExchange("order-event-exchange", true, false);

        return topicExchange;

    }


    /**
     * 创建绑定关系
     */
    @Bean
    public Binding orderCreateOrderBingDing() {

        //Binding.DestinationType.QUEUE 表示目的地类型
        //(String destination, DestinationType destinationType, String exchange, String routingKey,@Nullable Map<String, Object> arguments)
        return new Binding("order.delay.queue",
                Binding.DestinationType.QUEUE,
                "order-event-exchange",
                "order.create.order",
                null);


    }

    @Bean
    public Binding orderReleaseOrderBingDing() {
        return new Binding("order.release.order.queue",
                Binding.DestinationType.QUEUE,
                "order-event-exchange",
                "order.release.order",
                null);

    }


    /**
     * 订单交换机和库存交换机进行一个绑定，来实现一个联动
     *   订单释放直接和库存释放进行绑定
     *
     * */
    @Bean
    public Binding orderReleaseOtherBinding(){
        return new Binding("stock.release.stock.queue",
                Binding.DestinationType.QUEUE,
                "order-event-exchange",
                "order.release.other.#",
                null);

    }

}
