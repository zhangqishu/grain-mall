package com.zqs.grainmall.order.feign;


import com.zqs.grainmall.order.vo.OrderItemVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient("grainmall-cart")
public interface CartFeignService {
    @GetMapping("currentUserCartItems")
    public List<OrderItemVo> currentUserCartItems();

}
