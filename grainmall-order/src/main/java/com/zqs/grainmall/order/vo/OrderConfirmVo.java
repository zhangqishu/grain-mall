package com.zqs.grainmall.order.vo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-21 11:14
 **/

//订单确认页需要用的数据

public class OrderConfirmVo {

    @Setter @Getter
    //收获地址  ums_member_receive_address
    List<MemberAddressVo> memberAddressVos;

    @Setter @Getter
    //所有选中的购物项
    List<OrderItemVo> items;

    //发票记录....


    @Setter @Getter
    //优惠卷信息....
    Integer integration;

    @Setter @Getter
    Map<Long,Boolean> stocks;


    BigDecimal total; //订单总额

    public BigDecimal getTotal() {
        BigDecimal sum = new BigDecimal("0");
        if (items!=null) {
            for (OrderItemVo item : items) {
                BigDecimal multiply = item.getPrice().multiply(new BigDecimal(item.getCount().toString()));
                sum=sum.add(multiply);

            }
        }
        return sum;
    }

    @Setter @Getter
    String orderToken;  //订单的防重令牌


    public Integer getCount(){
        Integer i=0;
        if (items!=null) {
            for (OrderItemVo item : items) {
               i+=item.getCount();
            }
        }
        return i;
    }


  //  BigDecimal payPrice;  //应付价格

    public BigDecimal getPayPrice() {
        return getTotal();
    }
}
