package com.zqs.grainmall.order.constant;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-23 10:38
 **/
public class OrderConstant {
    public static final String USER_ORDER_TOKEN_PREFIX="order:token:";
}
