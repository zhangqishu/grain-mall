package com.zqs.grainmall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zqs.common.utils.PageUtils;
import com.zqs.grainmall.order.entity.RefundInfoEntity;

import java.util.Map;

/**
 * 退款信息
 *
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 18:35:44
 */
public interface RefundInfoService extends IService<RefundInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

