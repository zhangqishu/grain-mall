package com.zqs.grainmall.order.service.impl;

import com.rabbitmq.client.Channel;
import com.zqs.grainmall.order.entity.OrderEntity;
import com.zqs.grainmall.order.entity.OrderReturnReasonEntity;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zqs.common.utils.PageUtils;
import com.zqs.common.utils.Query;

import com.zqs.grainmall.order.dao.OrderItemDao;
import com.zqs.grainmall.order.entity.OrderItemEntity;
import com.zqs.grainmall.order.service.OrderItemService;


@RabbitListener(queues = {"hello-java-queue"})
@Service("orderItemService")
public class OrderItemServiceImpl extends ServiceImpl<OrderItemDao, OrderItemEntity> implements OrderItemService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderItemEntity> page = this.page(
                new Query<OrderItemEntity>().getPage(params),
                new QueryWrapper<OrderItemEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * queue: 声明需要监听到所有队列
     *
     * 参数可以写以下类型:
     *    1.Message message： 原生消息信息。头+体
     *    2.T<发送到消息的类型>: OrderReturnReasonEntity content,这样写的话我们就不用手动转化为对象，Spring帮我们自动转化
     *    3.Channel channel： 当前传输数据的通道 TODO
     *
     *  Queue： 可以很多人都来监听，但是只能有一个人收到，并且只要收到消息，队列就会自动删除消息
     *  场景:
     *      1.订单服务启动多个同时去接收消息，只能有一个服务收到
     *      2.只有一个消息完全处理完，方法运行结束，我们就可以接收下一个消息
     *
     * */

    @RabbitHandler
    public void recieveMessage(Message message, OrderReturnReasonEntity content, Channel channel) throws InterruptedException, IOException {
        System.out.println("接收到消息..内容...."+message+"==>内容"+content);
        byte[] body = message.getBody();
        //消息头属性信息
        MessageProperties properties = message.getMessageProperties();
       // Thread.sleep(3000);
        System.out.println("消息处理完成==》"+content.getName());
        //channel内按顺序自增的
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        System.out.println("deliveryTag==>"+deliveryTag);

        //签收消息,非批量签收模式
        try {
            /**
             * long deliveryTag
             * boolean multiple  是否批量删除
             * */
            if (deliveryTag%2==0) {
                //收货
                channel.basicAck(deliveryTag, false);
                System.out.println("签收了货物...."+deliveryTag);
            }else {
                //退货  requeue=false 丢弃 不回去了  requeue=true 发回服务器，服务器重新入队
                //long deliveryTag, boolean multiple, boolean requeue
                channel.basicNack(deliveryTag,false,false);
               // long deliveryTag, boolean requeue
               // channel.basicReject();
                System.out.println("没有签收了货物..."+deliveryTag);
            }
        }catch (Exception E)
        {
            //网络中断

        }


    }

    @RabbitHandler
    public void recieveMessage2(OrderEntity content) throws InterruptedException {

        System.out.println("接收到消息==》"+content);

    }

}