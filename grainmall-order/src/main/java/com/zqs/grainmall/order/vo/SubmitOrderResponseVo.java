package com.zqs.grainmall.order.vo;

import com.zqs.grainmall.order.entity.OrderEntity;
import lombok.Data;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-23 11:05
 **/

@Data
public class SubmitOrderResponseVo {

    private OrderEntity order;
    private Integer code; //0成功 错误状态码
}
