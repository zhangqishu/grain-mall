package com.zqs.grainmall.coupon.dao;

import com.zqs.grainmall.coupon.entity.SeckillSkuRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动商品关联
 * 
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 21:10:09
 */
@Mapper
public interface SeckillSkuRelationDao extends BaseMapper<SeckillSkuRelationEntity> {
	
}
