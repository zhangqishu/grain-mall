package com.zqs.grainmall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zqs.common.utils.PageUtils;
import com.zqs.grainmall.coupon.entity.SeckillPromotionEntity;

import java.util.Map;

/**
 * 秒杀活动
 *
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 21:10:10
 */
public interface SeckillPromotionService extends IService<SeckillPromotionEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

