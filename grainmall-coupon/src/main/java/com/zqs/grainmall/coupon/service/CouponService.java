package com.zqs.grainmall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zqs.common.utils.PageUtils;
import com.zqs.grainmall.coupon.entity.CouponEntity;

import java.util.Map;

/**
 * 优惠券信息
 *
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 21:10:10
 */
public interface CouponService extends IService<CouponEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

