package com.zqs.grainmall.coupon.dao;

import com.zqs.grainmall.coupon.entity.CategoryBoundsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品分类积分设置
 * 
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 21:10:10
 */
@Mapper
public interface CategoryBoundsDao extends BaseMapper<CategoryBoundsEntity> {
	
}
