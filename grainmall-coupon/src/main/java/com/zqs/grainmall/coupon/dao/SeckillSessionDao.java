package com.zqs.grainmall.coupon.dao;

import com.zqs.grainmall.coupon.entity.SeckillSessionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动场次
 * 
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 21:10:10
 */
@Mapper
public interface SeckillSessionDao extends BaseMapper<SeckillSessionEntity> {
	
}
