package com.zqs.grainmall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zqs.common.utils.PageUtils;
import com.zqs.grainmall.coupon.entity.SeckillSessionEntity;

import java.util.Map;

/**
 * 秒杀活动场次
 *
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 21:10:10
 */
public interface SeckillSessionService extends IService<SeckillSessionEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

