package com.zqs.grainmall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-10-15 21:13
 **/

/**
 *  1.如何使用Nacos作为配置中心统一管理配置
 *   1.1: 引入依赖
 *         <dependency>
 *             <groupId>com.alibaba.cloud</groupId>
 *             <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
 *         </dependency>
 *   1.2: 创建一个boostrap.properties/yml的全局配置文件,写入如下配置
 *        spring.application.name=grainmall-coupon
 *        spring.cloud.nacos.config.server-addr=127.0.0.1:8848
 *   1.3: 我们只需在nacos配置中心添加一个数据集(Data Id)   grainmall-coupon.properties: 微服务名.properties
 *   1.4: 配置完成后我们必须重启微服务配置才生效，我们需要动态获取配置，结合以下2个注解
 *     1.4.1: @RefreshScope 刷新配置
 *     1.4.1: @Value("${"配置项的名"}") 获取某个配置的值
 *     总结: 如果配置中心和当前应用的配置文件中都配置了相同的项那么优先使用配置中心(废话)
 *
 *  2.如何配置命名空间和配置分组
 *   2.1: 命名空间:
 *      2.1.1: 每个环境之间进行隔离。默认为public,默认新增的所有配置都在public空间
 *         2.1.1.1: 开发，测试，生产:利用命名空间来做隔离，我在nacos配置中心新建的prop命名空间，只需在bootstrap里添加以下配置，绑定它的id
 *           spring.cloud.nacos.config.namespace=47ac3b86-68df-4de4-b8b4-7271fbb85db0
 *
 *      2.1.2: 每一个微服务之间互相隔离配置，每一个微服务都创建自己的命名空间，只加载自己命名空间下的所有配置
 *
 *   2.2: 配置集:所有的配置集合
 *   2.3: 配置集id:类似文件名
 *      Data ID
 *   2.4: 配置分组:
 *      默认的所有配置集都属于:DEFAULT_GROUP,只需添加如下配置
 *      spring.cloud.nacos.config.group=xxx
 *
 *  总结: 每个微服务创建自己的命名空间，可以通过配置分组来区分环境:dev,test,prod
 *
 *  3.同时加载多个配置集
 *   3.1: 微服务任何配置信息，任何配置文件都可以放在配置中心中
 *   3.2: 只需要在bootstrap中说明加载配置中心中哪些配置即可
 *   3.3: 以前Springboot任何方式从配置文件中获取值的注解都可以使用(先加载nacos配置中心中的配置)
 *        比如@Value,@ConfigurationProperties
 *
 *
 *
 * */

@SpringBootApplication
@EnableDiscoveryClient
public class GrainmallCouponApplication {
    public static void main(String[] args) {
        SpringApplication.run(GrainmallCouponApplication.class, args);
    }

}
