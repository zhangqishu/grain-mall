package com.zqs.grainmall.coupon.dao;

import com.zqs.grainmall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author zhangqishu
 * @email 2540074917@qq.com
 * @date 2020-10-15 21:10:10
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
