package com.zqs.grainmall.thirdparty;

import com.aliyun.oss.OSSClient;

import com.zqs.grainmall.thirdparty.component.SmsComponent;
import com.zqs.grainmall.thirdparty.utils.HttpUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
class GrainmallThirdPartyApplicationTests {


    @Autowired
    OSSClient ossClient;

    @Autowired
    SmsComponent smsComponent;

    @Test
    public void testSendCode(){
        smsComponent.sendSmsCode("17683776487","are you ok?");
    }


    @Test
    public void sendSms(){
        String host = "https://smssend.shumaidata.com";
        String path = "/sms/send";
        String method = "POST";
        String appcode = "11aab8683ed240d7975c53bd77d96a4d";
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("receive", "18972068562");
        querys.put("tag", "猜猜我是谁");
        querys.put("templateId", "M09DD535F4");
        Map<String, String> bodys = new HashMap<String, String>();


        try {
            HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
            System.out.println(response.toString());
            //获取response的body
            System.out.println(EntityUtils.toString(response.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Test
    public void testUpload() throws FileNotFoundException {
        //上传文件流。
        InputStream inputStream = new FileInputStream("/Users/zhangqishu/Downloads/3175DC63DC8F767C43B0409D7E5FFD7D.png");

        ossClient.putObject("grainmall-zqs", "hhahah", inputStream);

        // 关闭OSSClient。
        ossClient.shutdown();

        System.out.println("上传成功");
    }
}
