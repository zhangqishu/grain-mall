package com.zqs.grainmall.search;

import com.alibaba.fastjson.JSON;
import com.zqs.grainmall.search.config.GrainmallElasticSearchConfig;
import lombok.Data;
import lombok.ToString;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.Avg;
import org.elasticsearch.search.aggregations.metrics.AvgAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

@SpringBootTest
class GrainmallSearchApplicationTests {

    @Autowired
    private RestHighLevelClient client;



    @ToString
    @Data
    static class Account {
        private int account_number;
        private int balance;
        private String firstname;
        private String lastname;
        private int age;
        private String gender;
        private String address;
        private String employer;
        private String email;
        private String city;
        private String state;
    }

    /**
     * {
     *     skuId:1
     *     spuId:1
     *     skuTitle:华为xx
     *     price:998
     *     saleCount:99
     *     attrs:[
     *     {尺寸：5寸}
     *     {CPU：高通945}
     *     {分辨率：全高清}
     *     ]
     * }
     *
     *
     * */

    @Test
    void contextLoads() {
        System.out.println(client);

    }
    /**
     * 检索数据
     *
     * */
    @Test
    public void searchData() throws IOException {
        //1.创建检索请求
        SearchRequest searchRequest=new SearchRequest();
        //指定在哪里检索
        searchRequest.indices("bank");
        //指定检索条件/DSL
        //searchRequestBuilder
        SearchSourceBuilder sourceBuilder=new SearchSourceBuilder();
        //1.1 构造检索条件
 //       sourceBuilder.query();
//        sourceBuilder.from();
//        sourceBuilder.size();
//        sourceBuilder.aggregation();

        sourceBuilder.query(QueryBuilders.matchQuery("address","mill"));

        //按照年龄的值分布进行聚合
        TermsAggregationBuilder ageAgg = AggregationBuilders.terms("ageAgg").field("age").size(10);
        sourceBuilder.aggregation(ageAgg);

        //计算平均薪资
        AvgAggregationBuilder balbance = AggregationBuilders.avg("balanceAvg").field("balance");
        sourceBuilder.aggregation(balbance);


        System.out.println("检索条件:"+sourceBuilder.toString());

        searchRequest.source(sourceBuilder);


        //2.执行检索
        SearchResponse searchResponse = client.search(searchRequest, GrainmallElasticSearchConfig.COMMON_OPTIONS);

        //3.分析结果  searchResponse
        //把结果封装成map输出
        Map map = JSON.parseObject(searchResponse.toString(), Map.class);
        System.out.println(map);

        //3.1 获取所有查到的记录
        //把结果封装到实体类中输出
        SearchHits hits = searchResponse.getHits();
        SearchHit[] searchHits = hits.getHits();
        for (SearchHit searchHit : searchHits) {
           // searchHit.getIndex(); searchHit.getId();
            String string = searchHit.getSourceAsString();
            Account account = JSON.parseObject(string, Account.class);
            System.out.println("account:"+account);

        }

        //3.2 获取这次检索的分析信息
        Aggregations aggregations = searchResponse.getAggregations();
//        for (Aggregation aggregation : aggregations.asList()) {
//            System.out.println("当前聚合:"+aggregation.getName());
//
//        }
        Terms ageAgg1 = aggregations.get("ageAgg");
        for (Terms.Bucket bucket : ageAgg1.getBuckets()) {
            String keyAsString = bucket.getKeyAsString();
            System.out.println("年龄:"+keyAsString+"==>"+bucket.getDocCount());
        }

        Avg balanceAvg = aggregations.get("balanceAvg");
        System.out.println("平均薪资:"+balanceAvg.getValue());


    }

    /**
     * 测试存储数据到es
     * 更新也可以
     * */
    @Test
    public void indexData() throws IOException {
        IndexRequest request = new IndexRequest("users");
        request.id("1");  //数据的id
        Users users = new Users();
        users.setUsername("顾沉舟");
        users.setGender("男");
        users.setAge(18);
        String jsonString = JSON.toJSONString(users);
        request.source(jsonString,XContentType.JSON);   //要保存的内容

        //执行操作
        IndexResponse index = client.index(request, GrainmallElasticSearchConfig.COMMON_OPTIONS);

        //提取有用的响应数据
        System.out.println(index);
    }

    @Data
    class Users{
            private String username;
            private String gender;
            private Integer age;
        }


    @Test
    public void teststring() {
        String  s="abcd";
        String[] s1 = s.split("_");
        System.out.println(s1.toString());

    }


}

