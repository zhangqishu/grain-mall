package com.zqs.grainmall.search.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-04 10:30
 **/

/**
 * 动静分离暂时不做148，静态资源放到tomcat ，nginx暂时不做,如果你静态资源放到了nginx,可以把这个类删了
 * */
@Configuration
public class MyThymeleafConfig implements WebMvcConfigurer {


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**")
                .addResourceLocations("classpath:/static/");
    }
}
