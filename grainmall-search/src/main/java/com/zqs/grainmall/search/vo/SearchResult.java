package com.zqs.grainmall.search.vo;

import com.zqs.common.to.es.SkuEsModel;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-10 16:19
 **/
@Data
public class SearchResult {

    //查询到的所有商品信息
    private List<SkuEsModel> products;


    /**
     * 分页信息
     *
     * */
    private Integer pageNum; //当前页面
    private Long total;   //总记录数
    private Integer totalPages; //总页码
    private List<Integer> pageNavs;  //导航页码

    private List<BrandVo> brands; //当前查询到的结果，所有涉及到的品牌信息
    private List<CatalogVo> catalogs; //当前查询到的结果,所有涉及到的分类信息
    private List<AttrVo> attrs;  //当前查询到的结果，所有涉及到的属性信息

    //=============以上的信息是要返回给页面的=================

    //面包屑导航数据
    private List<NavVo> navs=new ArrayList<>();
    private List<Long> attrIds=new ArrayList<>();

    @Data
    public static class NavVo{
        private String navName;
        private String navValue;
        private String link;


    }

    //品牌信息
    @Data
   public static class BrandVo{
        private Long brandId;
        private String brandName;
        private String brandImg;
    }

    @Data
    public static class CatalogVo{
        private Long catalogId;
        private String catalogName;
    }

    @Data
    public static class AttrVo{
        private Long attrId;
        private String attrName;
        private List<String> attrValue;
    }




}
