package com.zqs.grainmall.search.vo;

import lombok.Data;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-13 22:13
 **/

@Data
public class BrandVo {
    private Long brandId;
    private String brandName;
}
