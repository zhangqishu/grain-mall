package com.zqs.grainmall.search.service;

import com.zqs.common.to.es.SkuEsModel;

import java.io.IOException;
import java.util.List;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-03 09:57
 **/
public interface ProductSaveService {
    Boolean productStatusUp(List<SkuEsModel> skuEsModels) throws IOException;

}
