package com.zqs.grainmall.search.service.impl;

import com.alibaba.fastjson.JSON;
import com.zqs.common.to.es.SkuEsModel;
import com.zqs.grainmall.search.config.GrainmallElasticSearchConfig;
import com.zqs.grainmall.search.constant.EsConstant;
import com.zqs.grainmall.search.service.ProductSaveService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-03 10:25
 **/
@Slf4j
@Service
public class ProductSaveServiceImpl  implements ProductSaveService {

    @Autowired
    RestHighLevelClient restHighLevelClient;

    @Override
    public Boolean productStatusUp(List<SkuEsModel> skuEsModels) throws IOException {
        //保存到es中
        //1.给es中建立一个索引。 product并建立好映射关系

        //2.给es中保存这些数据
        //BulkRequest bulkRequest RequestOptions options
        BulkRequest bulkRequest = new BulkRequest();
        for (SkuEsModel model : skuEsModels) {
            //构造保存请求
            //创建索引
            IndexRequest indexRequest = new IndexRequest(EsConstant.PRODUCT_INDEX);
            //指定当前商品的skuId
            indexRequest.id(model.getSkuId().toString());
            //指定数据内容
            String s = JSON.toJSONString(model);
            indexRequest.source(s, XContentType.JSON);

            bulkRequest.add(indexRequest);
        }

        BulkResponse bulk = restHighLevelClient.bulk(bulkRequest, GrainmallElasticSearchConfig.COMMON_OPTIONS);

        //TODO 1.如果批量错误
        //是否执行错误
        boolean b = bulk.hasFailures();
        List<String> collect = Arrays.stream(bulk.getItems())
                //TODO
                .map(item->{
                    return item.getId();
                })
                .collect(Collectors.toList());
        log.info("商品上架完成:{},返回数据:{}",collect,bulk.toString());

        return b;


    }
}
