package com.zqs.grainmall.search.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.zqs.common.to.es.SkuEsModel;
import com.zqs.common.utils.R;
import com.zqs.grainmall.search.config.GrainmallElasticSearchConfig;
import com.zqs.grainmall.search.constant.EsConstant;
import com.zqs.grainmall.search.feign.ProductFeignService;
import com.zqs.grainmall.search.service.MallSearchService;
import com.zqs.grainmall.search.vo.AttrResponseVo;
import com.zqs.grainmall.search.vo.BrandVo;
import com.zqs.grainmall.search.vo.SearchParam;
import com.zqs.grainmall.search.vo.SearchResult;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.nested.NestedAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.nested.ParsedNested;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedLongTerms;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-10 15:56
 **/
@Service
public class MallSearchServiceImpl implements MallSearchService {

    @Autowired
    private RestHighLevelClient client;

    @Autowired
    private ProductFeignService productFeignService;

    //去es进行检索
    @Override
    public SearchResult search(SearchParam param) {
        //动态构建出查询需要的DSL语句
        SearchResult result=null;

        //1.准备检索请求
        SearchRequest searchRequest=buildSearchRequest(param);

        try {
            //2.执行检索请求
            org.elasticsearch.action.search.SearchResponse response = client.search(searchRequest, GrainmallElasticSearchConfig.COMMON_OPTIONS);

            //3.分析响应数据封装成我们需要的格式
            result=buildSearchResult(response,param);

        } catch (IOException e) {
            e.printStackTrace();
        }


        return result;
    }




    /**
     * 准备检索请求:
     *    模糊匹配，过滤(按照属性，分类，品牌，价格区间，库存)，排序，分页，高亮，聚合分析
     *
     * */
    private SearchRequest buildSearchRequest(SearchParam param) {

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder(); //帮助我们构建DSL语句的

        /**
         * 查询模糊匹配，过滤(按照属性，分类，品牌，价格区间，库存)
         *
         */
        //1.构建bool -query
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();

        //1.1  must
        if (!StringUtils.isEmpty(param.getKeyword())) {
            boolQuery.must(QueryBuilders.matchQuery("skuTitle", param.getKeyword()));

        }

        //1.2
        //1.2.1  filter  --按照3级分类的id查询
        if (!StringUtils.isEmpty(param.getCatalog3Id())) {
            boolQuery.filter(QueryBuilders.termQuery("catalogId", param.getCatalog3Id()));
        }
        //1.2.2  filter --按照品牌id查询
        if (param.getBrandId() != null && param.getBrandId().size() > 0) {
            boolQuery.filter(QueryBuilders.termsQuery("brandId", param.getBrandId()));
        }

        //1.2.3  filter --查询所指定的属性进行查询
        if (param.getAttrs() != null && param.getAttrs().size() > 0) {

            //attrs=1_5寸：8寸&attrs=2_16G：8G
            for (String attrStr : param.getAttrs()) {
                BoolQueryBuilder nestedboolQuery = QueryBuilders.boolQuery();
                //attr=1_5寸:8寸
                String[] s = attrStr.split("_");
                String attrId = s[0]; //检索的属性id
                String[] attrValues = s[1].split(":");  //这个属性的检索用的值
                nestedboolQuery.must(QueryBuilders.termQuery("attrs.attrId", attrId));

                //TODO bug打卡 原因: 把termsQuery写成了termQuery   ---张淇舒
                nestedboolQuery.must(QueryBuilders.termsQuery("attrs.attrValue", attrValues));

                //每一个必须都得生成一个nested查询
                NestedQueryBuilder nestedQuery = QueryBuilders.nestedQuery("attrs", nestedboolQuery, ScoreMode.None); //ScoreMode.None不参与评分

                boolQuery.filter(nestedQuery);
            }
        }

        //1.2.4  filter --查询是否有库存
        if (param.getHasStock()!=null) {
            boolQuery.filter(QueryBuilders.termQuery("hasStock", param.getHasStock() == 1));
        }

        //1.2.5  filter --按照价格区间
        if (!StringUtils.isEmpty(param.getSkuPrice())) {
            RangeQueryBuilder rangeQuery = QueryBuilders.rangeQuery("skuPrice");
            String[] s = param.getSkuPrice().split("_");
            if (s.length == 2) {
                //价格区间
                rangeQuery.gte(s[0]).lte(s[1]);
            } else if (s.length == 1) {

                //_500/500_
                if (param.getSkuPrice().startsWith("_")) {
                    //以"_"开始的  小于等于500
                    rangeQuery.lte(s[0]);
                }
                if (param.getSkuPrice().endsWith("_")) {
                    //以"_"结尾的  大于等于500
                    rangeQuery.gte(s[0]);

                }
            }
            boolQuery.filter(rangeQuery);

        }


        //把上面所有查询条件都拿来封装
        sourceBuilder.query(boolQuery);


        /**
         *
         * 排序，分页，高亮
         * */
        //2.1  排序
        if (!StringUtils.isEmpty(param.getSort()))
        {
            String sort=param.getSort();
            //sore=hotScore_asc/desc
           String[] s=sort.split("_");
           SortOrder order=s[1].equalsIgnoreCase("asc")?SortOrder.ASC:SortOrder.DESC;
           sourceBuilder.sort(s[0], order);
        }

        //2.2  分页   pageSize:
        //pageNum: 1  from:0  size:5  [0,1,2,3,4]
        //pageNum: 2  from:5  size:5
        //form=(pageNum-1)*size
        sourceBuilder.from((param.getPageNum()-1)*EsConstant.PRODUCT_PAGESIZE);
        sourceBuilder.size(EsConstant.PRODUCT_PAGESIZE);



        //2.3  高亮
        if (!StringUtils.isEmpty(param.getKeyword()))
        {
            HighlightBuilder builder = new HighlightBuilder();
            builder.field("skuTitle");
            builder.preTags("<b style='color:red'>");
            builder.postTags("</b>");
            sourceBuilder.highlighter(builder);
        }



        /**
         * 聚合分析
         *
         * */
        //3.1  品牌聚合
        TermsAggregationBuilder brand_agg = AggregationBuilders.terms("brand_agg");
        brand_agg.field("brandId");
        brand_agg.size(50);
        //3.1.1  品牌聚合的子聚合
        brand_agg.subAggregation(AggregationBuilders.terms("brand_name_agg").field("brandName").size(10));
        brand_agg.subAggregation(AggregationBuilders.terms("brand_img_agg").field("brandImg").size(10));

        //TODO 1.聚合品牌信息-->brand_agg
        sourceBuilder.aggregation(brand_agg);

        //3.2   分类聚合
        TermsAggregationBuilder catalog_agg=AggregationBuilders.terms("catalog_agg");
        catalog_agg.field("catalogId");
        catalog_agg.size(10);
        //3.2.1  分类子聚合
        catalog_agg.subAggregation(AggregationBuilders.terms("catalog_name_agg").field("catalogName.keyword").size(10));

        //TODO 分类的聚合 --> catalog_agg
        sourceBuilder.aggregation(catalog_agg);

        //3.3   属性聚合 attr_agg
        NestedAggregationBuilder attr_agg = AggregationBuilders.nested("attr_agg", "attrs");

        //3.3.1  属性子聚合  attr_id_agg
        //聚合出当前所有的attrId
        TermsAggregationBuilder attr_id_agg = AggregationBuilders.terms("attr_id_agg").field("attrs.attrId").size(10);
        //聚合分析出当前attr_id对应的名字
        attr_id_agg.subAggregation(AggregationBuilders.terms("attr_name_agg").field("attrs.attrName").size(10));
        //聚合分析出当前attr_id对应的所有可能的属性值attrValue
        attr_id_agg.subAggregation(AggregationBuilders.terms("attr_value_agg").field("attrs.attrValue").size(10));

        attr_agg.subAggregation(attr_id_agg);

        //TODO  属性信息的聚合 -->attr_agg
        sourceBuilder.aggregation(attr_agg);


        String s = sourceBuilder.toString();
        System.out.println("构建的dsl语句"+s);

        //索引  构建的dsl语句
        SearchRequest searchRequest = new SearchRequest(new String[]{EsConstant.PRODUCT_INDEX}, sourceBuilder);

        return searchRequest;

    }

    /**
     *
     * 构建结果数据
     *
     * */
    private SearchResult buildSearchResult(SearchResponse response,SearchParam param) {

        SearchResult result = new SearchResult();

        //1.返回的所有查询到的商品
        SearchHits hits = response.getHits();
        List<SkuEsModel> esModels = new ArrayList<>();
        if (hits.getHits() != null && hits.getHits().length > 0) {
            for (SearchHit hit : hits.getHits()) {
                String sourceAsString = hit.getSourceAsString();
                //把sourceAsString字符串转为SkuEsModel对象
                SkuEsModel esModel = JSON.parseObject(sourceAsString, SkuEsModel.class);
                if (!StringUtils.isEmpty(param.getKeyword())) {
                    HighlightField skuTitle = hit.getHighlightFields().get("skuTitle");
                    String string = skuTitle.getFragments()[0].string();
                    esModel.setSkuTitle(string);
                }
                esModels.add(esModel);
            }
        }
        result.setProducts(esModels);




        //2.当前商品涉及到的所有属性信息
        List<SearchResult.AttrVo> attrVos = new ArrayList<>();
        ParsedNested attr_agg = response.getAggregations().get("attr_agg");
        ParsedLongTerms attr_id_agg = attr_agg.getAggregations().get("attr_id_agg");
        for (Terms.Bucket bucket : attr_id_agg.getBuckets()) {
            SearchResult.AttrVo attrVo = new SearchResult.AttrVo();

            //1.得到属性的id
            long attrId = bucket.getKeyAsNumber().longValue();

            //2.得到属性的名字
            String attrName = ((ParsedStringTerms) bucket.getAggregations().get("attr_name_agg")).getBuckets().get(0).getKeyAsString();

            //3.得到属性的所有值
            List<String> attrValues = ((ParsedStringTerms) bucket.getAggregations().get("attr_value_agg")).getBuckets().stream().map(item -> {
                //我们的buckets里面可能有多个值，所以挨个遍历一遍返回一个string封装到一个集合里面
                String keyAsString = item.getKeyAsString();
                return keyAsString;
            }).collect(Collectors.toList());

            attrVo.setAttrId(attrId);
            attrVo.setAttrName(attrName);
            attrVo.setAttrValue(attrValues);

            attrVos.add(attrVo);
        }

        result.setAttrs(attrVos);

//        //3.当前商品涉及到的所有品牌信息
        List<SearchResult.BrandVo> brandVos = new ArrayList<>();
        ParsedLongTerms brand_agg = response.getAggregations().get("brand_agg");
        for (Terms.Bucket bucket : brand_agg.getBuckets()) {
            SearchResult.BrandVo brandVo = new SearchResult.BrandVo();

            //1.得到品牌的id
            long brandId = bucket.getKeyAsNumber().longValue();

            //2.得到品牌的名字
            String brandName = ((ParsedStringTerms) bucket.getAggregations().get("brand_name_agg")).getBuckets().get(0).getKeyAsString();

            //3.得到品牌的图片
            String brandImg = ((ParsedStringTerms) bucket.getAggregations().get("brand_img_agg")).getBuckets().get(0).getKeyAsString();

            brandVo.setBrandId(brandId);
            brandVo.setBrandName(brandName);
            brandVo.setBrandImg(brandImg);

            brandVos.add(brandVo);
        }
        result.setBrands(brandVos);


//
//        //4.当前所有商品涉及到到所有分类信息
        List<SearchResult.CatalogVo> catalogVos = new ArrayList<>();
        ParsedLongTerms catalog_agg = response.getAggregations().get("catalog_agg");
        List<? extends Terms.Bucket> buckets = catalog_agg.getBuckets();
        for (Terms.Bucket bucket : buckets) {
            SearchResult.CatalogVo catalogVo = new SearchResult.CatalogVo();
            //得到分类id
            String keyAsString = bucket.getKeyAsString();
            catalogVo.setCatalogId(Long.parseLong(keyAsString));

            //得到分类名
            ParsedStringTerms catalog_name_agg = bucket.getAggregations().get("catalog_name_agg");
            String catalog_name = catalog_name_agg.getBuckets().get(0).getKeyAsString();
            catalogVo.setCatalogName(catalog_name);
            catalogVos.add(catalogVo);
        }
        result.setCatalogs(catalogVos);
//
//     =====以上可以从聚合信息中获取=====
        //5.分页信息 -页码

        result.setPageNum(param.getPageNum());

        //6.分页信息 -总记录树
        long total = hits.getTotalHits().value;
        result.setTotal(total);
        //7. 分页信息 -总页码  计算得到 11/2=5...1
        int totalPages = (int) (total % EsConstant.PRODUCT_PAGESIZE == 0 ? total / EsConstant.PRODUCT_PAGESIZE : (total / EsConstant.PRODUCT_PAGESIZE + 1));
        result.setTotalPages(totalPages);


        List<Integer> pageNavs = new ArrayList<>();
        for (int i = 0; i < totalPages; i++) {
            pageNavs.add(i);

        }
        result.setPageNavs(pageNavs);


        //8.构建面包屑导航功能
        if (param.getAttrs() != null && param.getAttrs().size() > 0) {
            List<SearchResult.NavVo> collect = param.getAttrs().stream().map(attr -> {
                //分析每一个attrs传过来的查询参数值
                SearchResult.NavVo navVo = new SearchResult.NavVo();
                //attrs=2_5寸：6寸
                String[] s = attr.split("_");
                navVo.setNavValue(s[1]);

                //根据id远程调用feign的名字
                R r = productFeignService.attrInfo(Long.parseLong(s[0]));
                result.getAttrIds().add(Long.parseLong(s[0]));
                if (r.getCode() == 0) {
                    AttrResponseVo data = r.getData("attr", new TypeReference<AttrResponseVo>() {
                    });

                    navVo.setNavName(data.getAttrName());
                } else {
                    navVo.setNavName(s[0]);
                }

                //2.取消了这个面包屑之后，我们要跳转到那个地方，将请求地址到url里面的地址置空
                //拿到所有的查询条件，去掉当前。
                //attrs= 15_海思(Hisilicon)
                String replace = replaceQueryString(param, attr,"attrs");
                navVo.setLink("http://127.0.0.1:12000/list.html?" + replace);

                return navVo;

            }).collect(Collectors.toList());

            result.setNavs(collect);

        }

        //品牌，分类
        if (param.getBrandId()!=null&&param.getBrandId().size()>0)
        {
            List<SearchResult.NavVo> navs = result.getNavs();
            SearchResult.NavVo navVo = new SearchResult.NavVo();

            navVo.setNavName("品牌");

            //TODO 远程查询所有品牌
            R r = productFeignService.brandsInfo(param.getBrandId());
            if (r.getCode()==0) {
                List<BrandVo> brand = r.getData("brand", new TypeReference<List<BrandVo>>() {
                });

                StringBuffer buffer=new StringBuffer();
                String replace=null;
                for (BrandVo brandVo : brand) {
                    buffer.append(brandVo.getBrandName()+";");
                    replace = replaceQueryString(param, brandVo.getBrandId()+"","brandId");
                }
                navVo.setNavValue(buffer.toString());
                navVo.setLink("http://127.0.0.1:12000/list.html?" + replace);
            }

            navs.add(navVo);


        }

        //TODO 分类: 不需要导航取消

            return result;

    }

    private String replaceQueryString(SearchParam param, String value,String key) {
        String encode=null;
        try {
            //请求路径的编码设置为utf8
            encode = URLEncoder.encode(value, "UTF-8");
            encode = encode.replace("+","%20");  //浏览器对空格编码和Java不一样
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return param.get_queryString().replace("&" +key+ "=" + encode, "");
    }


}
