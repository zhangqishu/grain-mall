package com.zqs.grainmall.search.controller;

import com.zqs.grainmall.search.service.MallSearchService;
import com.zqs.grainmall.search.vo.SearchParam;
import com.zqs.grainmall.search.vo.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-10 11:44
 **/
@Controller
public class SearchController {

    @Autowired
    MallSearchService mallSearchService;

    /**
     *
     * 由SpringMvc自动把页面提交过来的所有查询请求参数封装成一个指定对象，也就是我封装的一个SearchParam
     *
     * */
    @GetMapping("/list.html")
    public String listPage(SearchParam param, Model model, HttpServletRequest request){

        String queryString = request.getQueryString();
        param.set_queryString(queryString);

        //1.根据传递来的页面的查询参数，去es中检索商品
        SearchResult result= mallSearchService.search(param);
        model.addAttribute("result",result);

        return "list";
    }
}
