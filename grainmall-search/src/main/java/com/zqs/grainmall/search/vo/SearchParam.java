package com.zqs.grainmall.search.vo;

import lombok.Data;

import java.util.List;

/**
 * @program: grain-mall
 * @description:  封装页面所有可能传过来的查询条件
 * @author: Mr.Zhang
 * @create: 2020-11-10 15:54
 **/

@Data
public class SearchParam {


    /**
     * 这种思路就是封装一个vo接收前端传过来的所有查询条件参数
     *
     * catalog3Id=225&keyword=小米&sort=saleCount&hasStock=0/1
     *
     * */

    private String keyword; //页面传递过来的全文匹配关键字(搜索框输入的)

    private Long catalog3Id; //三级分类id

    /**
     *排序条件:
     *  销量:
     *  sort=saleCount_asc/desc
     *  价格:
     *  sort=skuPrice_asc/desc
     *  热度:
     *  sort=hotScore_asc/desc
     * */
    private String sort;

    /**
     * 好多过滤条件
     *   hasStock(是否有货)、skuPrice（价格区间)、branId(根据品牌id查，可以多选)、catalog3Id、attrs(按照属性来筛选)
     *   hasStock=0/1  0无库存/1有库存
     *   skuPrice=1_500/_500/500_
     *   brandId=1&2&3
     *   attrs=2_5寸&6寸
     *
     *
     * */
    private Integer hasStock;
    private String skuPrice;
    private List<Long> brandId;
    private List<String> attrs;


    private Integer pageNum=1;  //页码

    private String _queryString; //原生的所有查询条件


}
