package com.zqs.grainmall.search.constant;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-03 10:28
 **/
public class EsConstant {
    public static final String PRODUCT_INDEX="grainmall_product";  //sku数据在es中的索引

    public static final Integer PRODUCT_PAGESIZE=16;  //sku数据在es中的索引

}
