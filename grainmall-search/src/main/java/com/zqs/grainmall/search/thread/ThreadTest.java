package com.zqs.grainmall.search.thread;

import java.util.concurrent.*;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-14 08:37
 **/

public class ThreadTest {
    //当前系统中池有一两个，每个异步任务，提交给线程池让他自己去执行
    public static ExecutorService executor = Executors.newFixedThreadPool(10);


    public static void main(String[] args) throws ExecutionException, InterruptedException {
        System.out.println("main...start....");
//        CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
//            System.out.println("当前线程" + Thread.currentThread().getId());
//            int i = 10 / 2;
//            System.out.println("运行结果" + i);
//
//        }, executor);

        //方法完成后的感知
//        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
//            System.out.println("当前线程" + Thread.currentThread().getId());
//            int i = 10 / 0;
//            System.out.println("运行结果" + i);
//            return i;
//        }, executor).whenComplete((res,exception)->{
//            //whenComplete虽然可以得到异常信息，但是没法修改返回数据
//            System.out.println("异步任务成功完成了...结果是:"+res+";异常是:"+exception);
//        }).exceptionally(throwable -> {
//            //可以感知异常，同时返回默认值
//            return 10;
//        });

        //方法执行完成后的处理
//        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
//            System.out.println("当前线程" + Thread.currentThread().getId());
//            int i = 10 / 4;
//            System.out.println("运行结果" + i);
//            return i;
//        }, executor).handle((res,thr)->{
//            if (res!=null)
//            {
//                return res*2;
//            }
//            if (thr!=null)
//            {
//                return 0;
//            }
//            return 0;
//        });
//        Integer integer = future.get();
//
//        System.out.println("main...end..." + integer);

        //1.thenRun： 不能获取到上一步的执行结果，无返回值
        /**
         * thenRunAsync(() -> {
         *             //如果上面的一个任务抛异常了，任务2不会执行
         *             System.out.println("任务2启动了");
         *         },executor);
         *
         * */
        //2.thenAcceptAsync 能接收上一步的结果，但是无返回值
        /**
         * thenAcceptAsync(res->{
         *             System.out.println("任务2启动了...."+res);
         *         },executor);
         *
         * */
        //3.thenApplyAsync 能接收上一步的结果，但是有返回值
//        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
//            System.out.println("当前线程" + Thread.currentThread().getId());
//            int i = 10 / 2;
//            System.out.println("运行结果" + i);
//            return i;
//        }, executor).thenApplyAsync(res -> {
//            System.out.println("任务2启动了...." + res);
//            try {
//                Thread.sleep(3000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            return res+2;
//
//        }, executor);
//
//        //future.get()是一个阻塞方法，再强调一遍，他是等着上面的方法执行完再执行
//        Integer integer = future.get();
//        System.out.println("main...end..."+integer);

//        CompletableFuture<Object> future01 = CompletableFuture.supplyAsync(() -> {
//            System.out.println("任务1线程" + Thread.currentThread().getId());
//            int i = 10 / 2;
//            System.out.println("任务1结果" + i);
//            return i;
//        }, executor);
//
//        CompletableFuture<Object> future02 = CompletableFuture.supplyAsync(() -> {
//            System.out.println("任务2线程" + Thread.currentThread().getId());
//
//            try {
//                Thread.sleep(3000);
//                System.out.println("任务2结果");
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            return "Hello";
//        }, executor);

//        future01.runAfterBothAsync(future02,()->{
//            System.out.println("任务3开始");
//        },executor);
//
//        future01.thenAcceptBothAsync(future02,(f1,f2)->{
//            System.out.println("任务3开始....之前的结果:"+f1+"-->"+f2);
//        },executor);
//
//        CompletableFuture<String> future = future01.thenCombineAsync(future02, (f1, f2) -> {
//            return f1 + ":" + f2 + "-> Hello";
//        }, executor);
//        System.out.println("main...end---"+future.get());

        //两任务组合一个完成，我们就执行任务3
//        future01.runAfterEitherAsync(future02,()->{
//            System.out.println("任务3开始...之前的结果");
//        },executor);
//
//        System.out.println("main....end...");

//        future01.acceptEitherAsync(future02,(res)->{
//
//            System.out.println("任务3开始...之前的结果"+res);
//
//        },executor);
//        System.out.println("main....end...");

//        CompletableFuture<String> future03 = future01.applyToEitherAsync(future02, (res) -> {
//            System.out.println("任务3开始....之前的结果:" + res);
//            return res.toString() + "->呵呵";
//        }, executor);
//
//        System.out.println("main....end..."+future03.get());

        CompletableFuture<String> futureImg = CompletableFuture.supplyAsync(() -> {
            System.out.println("查询商品的图片信息");
            return "hello.jpg";
        },executor);

        CompletableFuture<String> futureAttr = CompletableFuture.supplyAsync(() -> {
            System.out.println("查询商品的属性");
            return "黑色+256G";
        },executor);

        CompletableFuture<String> futureDesc = CompletableFuture.supplyAsync(() -> {

            try {
                Thread.sleep(3000);
                System.out.println("查询商品介绍");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "华为";
        },executor);

        CompletableFuture<Object> anyOf = CompletableFuture.anyOf(futureImg, futureAttr, futureDesc);
        //anyOf.get(); //等待所有结果完成
       // System.out.println("main...end"+futureImg.get()+futureAttr.get()+futureDesc.get());  //执行完之后获取返回值

        System.out.println("main...end"+anyOf.get());
    }


    public void thread(String[] args) throws ExecutionException, InterruptedException {

        System.out.println("main....start.....");

//        FutureTask<Integer> futureTask = new FutureTask<>(new Callable01());
//        new Thread(futureTask).start();
//        System.out.println("正在执行.....");
//        Thread.sleep(3000);
//        //阻塞  等待整个线程执行完成，获取返回结果
//        Integer integer = futureTask.get();
//        System.out.println("main...and...."+integer);

        //原生创建线程池
        /**
         *线程池的7大参数
         * int corePoolSize : [5]  核心线程数[一直存在除非设置了allowCoreThreadTimeOut]，线程池创建好以后就准备就绪的线程，就等待来接收异步任务来执行
         *        相当于new了5个空闲线程 Thread thread= new Thread();  thread.start();
         *
         * int maximumPoolSize: [200] 最大线程数量; 控制资源
         *
         * long keepAliveTime:  存活时间。如果当前的线程数量大于核心数量。
         *        释放空闲的线程(maximumPoolSize-corePoolSize)。只要线程空闲大于指定的keepAliveTime
         *
         * TimeUnit unit: 时间单位
         * BlockingQueue<Runnable> workQueue : 阻塞队列，如果任务有很多，就会将目前多的任务放在队列里面
         *        只要有线程空闲，就会去队列里面取出新的任务继续执行
         *
         * ThreadFactory threadFactory: 线程的创建工厂
         *
         * RejectedExecutionHandler handler: 如果队列满了，按照我们指定的拒绝策略拒绝执行任务
         *
         * 工作顺序:
         *    1. 线程池创建，准备好core数量的核心线程，准备接收任务
         *    1.1. 如果core满了，就将再进来的任务放入阻塞队列中，空闲的core就会自己去阻塞队列获取任务执行
         *    1.2. 如果阻塞队列满了，就直接开新线程执行，最大只能开到max指定的数量
         *    1.3. max满了就用RejectedExecutionHandler拒绝任务
         *    1.4. max都执行完成，有很多空闲，在指定的时间keepAliveTime以后，释放max-core这些线程
         *
         *            new LinkedBlockingDeque<>()默认是Integer的最大值。这会导致内存不够
         *
         * 面试题: 一个线程池 core 7; max 20, queue: 50,  100并发进来怎么分配？
         *          7个core会立即执行，50会进入队列，再开13个进行执行。剩下的30个就使用拒绝策略。
         *          如果不想抛弃还要执行。CallerRunsPolicy;
         *
         *
         * Future: 可以获取到异步结果
         * */
        ThreadPoolExecutor executor = new ThreadPoolExecutor(5,
                200,
                10,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(100000),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());


//        Executors.newCachedThreadPool();  //core是0，所有都可回收
//        Executors.newFixedThreadPool();  //固定大小  core=max;都不可回收
//        Executors.newScheduledThreadPool(); //做定时任务的线程池
//        Executors.newSingleThreadExecutor(); //单线程的线程池，后台从队列里面获取任务挨个执行

        // executor.submit(new Callable01());
        System.out.println("main...end....");


    }

    public static class Callable01 implements Callable<Integer> {

        @Override
        public Integer call() throws Exception {
            System.out.println("当前线程" + Thread.currentThread().getId());
            int i = 10 / 2;
            System.out.println("运行结果" + i);
            return i;
        }
    }
}
