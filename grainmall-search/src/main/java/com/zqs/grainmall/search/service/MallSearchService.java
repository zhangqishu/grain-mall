package com.zqs.grainmall.search.service;

import com.zqs.grainmall.search.vo.SearchParam;
import com.zqs.grainmall.search.vo.SearchResult;

public interface MallSearchService {

    /**
     * 检索所有参数
     * 返回检索的结果,里面包含页面需要的所有信息
     *
     * */
    SearchResult search(SearchParam param);
}
