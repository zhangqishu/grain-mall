package com.zqs.grainmall.search.controller;

import com.zqs.common.exception.BizCodeEnums;
import com.zqs.common.to.es.SkuEsModel;
import com.zqs.common.utils.R;
import com.zqs.grainmall.search.service.ProductSaveService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-03 09:48
 **/

@Slf4j
@RequestMapping("/search/save")
@RestController
public class ElasticSaveController {

    @Autowired
    ProductSaveService productSaveService;

    //1.上架商品
    @PostMapping("/product")
    public R productStatusUP(@RequestBody List<SkuEsModel> skuEsModels) {

        Boolean b =false;
        try
        {
           b=productSaveService.productStatusUp(skuEsModels);
        }catch (Exception e)
        {
            log.error("ElasticSaveController商品上架错误: {}",e);
            return R.error(BizCodeEnums.PRODUCT_UP_EXCEPTION.getCode(),BizCodeEnums.PRODUCT_UP_EXCEPTION.getMsg());
        }
        if (!b)
        {
            return R.ok();
        }else
        {
            return R.error(BizCodeEnums.PRODUCT_UP_EXCEPTION.getCode(),BizCodeEnums.PRODUCT_UP_EXCEPTION.getMsg());
        }



    }

}
