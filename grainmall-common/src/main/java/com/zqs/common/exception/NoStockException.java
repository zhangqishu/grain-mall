package com.zqs.common.exception;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-24 19:49
 **/
@Data
public class NoStockException extends RuntimeException {
    @Getter @Setter
    private Long skuId;

    public NoStockException(Long skuId){
        super("商品id:"+skuId+"没有足够的库存了");
    }

    public NoStockException(String msg) {
        super(msg);
    }



}
