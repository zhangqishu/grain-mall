package com.zqs.common.constant;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-18 22:35
 **/
public class CartConstant {
    public static final String TEMP_USER_COOKIE_NAME="user-key";

    public static final int TEMP_USER_COOKIE_TIMEOUT=60*60*24*30;
}
