package com.zqs.common.constant;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-17 10:03
 **/
public class AuthServerConstant {
    public static final String SMS_CODE_CACHE_PREFIX="sms:code:";
    public static final String LOGIN_USER="loginUser";
}
