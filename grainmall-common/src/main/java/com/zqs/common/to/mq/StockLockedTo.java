package com.zqs.common.to.mq;

import lombok.Data;

import java.util.List;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-27 15:13
 **/

@Data
public class StockLockedTo {

    private Long id;  //库存工作单的id
    private StockDetailTo detail; //工作详情的所有id


}
