package com.zqs.common.to;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-10-28 15:54
 **/
@Data
public class SkuReductionTo {

    private Long skuId;
    private int fullCount;
    private BigDecimal discount;
    private int countStatus;
    private BigDecimal fullPrice;
    private BigDecimal reducePrice;
    private int priceStatus;

    private List<MemberPrice> memberPrice;
}
