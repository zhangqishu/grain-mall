package com.zqs.common.valid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.Set;

public class ListValueConstraintValidator implements ConstraintValidator<ListValue,Integer> {

    private Set<Integer> set=new HashSet<>();

    //初始化方法
    @Override
    public void initialize(ListValue constraintAnnotation) {
        int[] vals = constraintAnnotation.vals();
        for (int val : vals) {
            set.add(val);
        }

    }

    //判断是否校验成功

    /**
     * @param  value   为你提交的那个值，也就是需要校验的值
     * @param  context 上下文环境信息
     * @return false
     * */
    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {

        //set.contains 表示set集合里面有没有包含某个值，返回boolean类型
        return set.contains(value);
    }
}
