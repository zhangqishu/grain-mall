package com.zqs.grainmall.cart.controller;

import com.zqs.common.constant.AuthServerConstant;
import com.zqs.grainmall.cart.interceptor.CartInterceptor;
import com.zqs.grainmall.cart.service.CartService;
import com.zqs.grainmall.cart.vo.Cart;
import com.zqs.grainmall.cart.vo.CartItem;
import com.zqs.grainmall.cart.vo.UserInfoTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-18 22:08
 **/
@Controller
public class CartController {

    @Autowired
    CartService cartService;

    @GetMapping("currentUserCartItems")
    @ResponseBody
    public List<CartItem> currentUserCartItems(){

        return cartService.getUserCartItems();
    }

    @GetMapping("/deleteItem")
    public String deleteItem(@RequestParam("skuId") Long skuId)
    {
        cartService.deleteItem(skuId);

        return "redirect:http://127.0.0.1:30000/cart.html";

    }

    @GetMapping("/countItem")
    public String countItem(@RequestParam("skuId") Long skuId,@RequestParam("num") Integer num)
    {
        cartService.checkItemCount(skuId,num);

        return "redirect:http://127.0.0.1:30000/cart.html";

    }

    @GetMapping("/checkItem")
    public String checking(@RequestParam("skuId") Long skuId,@RequestParam("checked") Integer checked)
    {
        cartService.checkItem(skuId,checked);
        return "redirect:http://127.0.0.1:30000/cart.html";

    }


    /**
     *
     * 浏览器有一个cookie: user-key: 标识用户身份，一个月后过期
     * 如果第一次使用jd的购物车功能，都会给一个临时的用户身份
     * 浏览器以后保存，每次都会访问带上这个cookie
     *
     * 登陆: session有
     * 没登陆: 按照cookie里面带来user-key来做
     * 第一次，如果没有临时用户，帮忙创建一个临时用户。
     * 跳转到购物车列表页
     *
     * */
    @GetMapping("/cart.html")
    public String cartListPage(Model model) throws ExecutionException, InterruptedException {

        //1.快速得到用户信息，id,user-key
       // UserInfoTo userInfoTo = CartInterceptor.threadLocal.get(); //TODO 利用ThreadLocal来共享一个线程里的数据

         Cart cart=cartService.getCart();
         model.addAttribute("cart",cart);

        return "cartList";


    }


    /**
     *
     * //RedirectAttributes ra重定向携带数据
     *         ra.addFlashAttribute(); 将数据放到session里面可以在页面取出，但是只能取出一次
     *         ra.addAttribute(); 将数据放到url后面
     * 添加商品到购物车
     * */
    @GetMapping("/addToCart")
    public String addToCart(@RequestParam("skuId") Long skuId,
                            @RequestParam("num") Integer num,
                            RedirectAttributes redirectAttributes) throws ExecutionException, InterruptedException {
         cartService.addToCart(skuId,num);


        redirectAttributes.addAttribute("skuId",skuId);
        return "redirect:http://127.0.0.1:30000/addToCartSuccess.html";

    }


    /**
     *跳转到成功页
     *
     * */
    @GetMapping("/addToCartSuccess.html")
    public String addToCartSuccessPage(@RequestParam("skuId") Long skuId, Model model){
        //重定向到成功页面。再次查询购物车数据即可
        CartItem item=cartService.getCartItem(skuId);
        model.addAttribute("cartItem",item);

        return "success";
    }


}
