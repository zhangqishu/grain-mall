package com.zqs.grainmall.cart.service;

import com.zqs.grainmall.cart.vo.Cart;
import com.zqs.grainmall.cart.vo.CartItem;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-18 21:56
 **/
public interface CartService {

    CartItem addToCart(Long skuId, Integer num) throws ExecutionException, InterruptedException;

    CartItem getCartItem(Long skuId);


    /**
     * 获取整个购物车
     * */
    Cart getCart() throws ExecutionException, InterruptedException;


    /**
     *
     * 清空购物车数据
     * */
    void clearCart(String cartkey);



    /**
     * 勾选购物项
     * */
    void checkItem(Long skuId, Integer checked);


    /**
     * 修改购物项数量
     * */
    void checkItemCount(Long skuId, Integer num);


    /**
     * 删除购物项
     *
     * */
    void deleteItem(Long skuId);

    List<CartItem> getUserCartItems();

}
