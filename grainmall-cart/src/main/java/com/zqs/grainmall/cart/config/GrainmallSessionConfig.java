package com.zqs.grainmall.cart.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-18 08:47
 **/

/**
 * 解决什么问题?
 * 1.默认发的令牌:session=asdasd。作用域:当前域(解决子域之间session共享问题)
 * 2.redis序列化
 *
 * */
@EnableRedisHttpSession
@Configuration
public class GrainmallSessionConfig {
    //修改session的作用域
    @Bean
    public CookieSerializer cookieSerializer(){
        DefaultCookieSerializer cookieSerializer = new DefaultCookieSerializer();
        //cookieSerializer.setDomainName("grainmall.com");
       // cookieSerializer.setDomainNamePattern("127.0.0.1");
        cookieSerializer.setCookieName("GULISESSION");

        return cookieSerializer;
    }

    @Bean
    public RedisSerializer<Object> springSessionDefaultRedisSerializer() {
        return new GenericJackson2JsonRedisSerializer();
    }
}
