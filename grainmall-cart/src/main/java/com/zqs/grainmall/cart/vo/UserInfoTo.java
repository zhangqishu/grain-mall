package com.zqs.grainmall.cart.vo;

import lombok.Data;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-18 22:30
 **/
@Data
public class UserInfoTo {
    private Long userId;
    private String userkey;

    private boolean tempUser=false;
}
