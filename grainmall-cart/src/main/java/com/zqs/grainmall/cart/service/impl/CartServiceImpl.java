package com.zqs.grainmall.cart.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.zqs.common.utils.R;
import com.zqs.grainmall.cart.feign.ProductFeignService;
import com.zqs.grainmall.cart.interceptor.CartInterceptor;
import com.zqs.grainmall.cart.service.CartService;
import com.zqs.grainmall.cart.vo.Cart;
import com.zqs.grainmall.cart.vo.CartItem;
import com.zqs.grainmall.cart.vo.SkuInfoVo;
import com.zqs.grainmall.cart.vo.UserInfoTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

/**
 * @program: grain-mall
 * @description:
 * @author: Mr.Zhang
 * @create: 2020-11-18 21:57
 **/
@Service
public class CartServiceImpl implements CartService {
    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    ProductFeignService productFeignService;

    @Autowired
    ThreadPoolExecutor executor;


    private final String CART_PZREFIX="granmall:cart:";
    @Override
    public CartItem addToCart(Long skuId, Integer num) throws ExecutionException, InterruptedException {

        BoundHashOperations<String, Object, Object> cartOps = getCartOps();

        String res = (String) cartOps.get(skuId.toString());
        if (StringUtils.isEmpty(res))
        {
            //购物车无此商品
            //2.添加新商品到购物车
            CartItem cartItem=new CartItem();
            CompletableFuture<Void> getSkuInfoTask = CompletableFuture.runAsync(() -> {
                //1.远程查询当前要添加的商品的信息
                R skuInfo = productFeignService.getSkuInfo(skuId);
                SkuInfoVo data = skuInfo.getData("skuInfo", new TypeReference<SkuInfoVo>() {
                });

                cartItem.setCheck(true);
                cartItem.setCount(num);
                cartItem.setImages(data.getSkuDefaultImg());
                cartItem.setTitle(data.getSkuTitle());
                cartItem.setSkuId(skuId);
                cartItem.setPrice(data.getPrice());

            }, executor);

            //3.远程查询sku的组合信息
            CompletableFuture<Void> getSkuSaleAttrValues = CompletableFuture.runAsync(() -> {
                List<String> values = productFeignService.getSkuSaleAttrValues(skuId);
                //4.attr信息添加到购物车
                cartItem.setSkuAttr(values);

            }, executor);

            //等这2个任务完成后再返回cartItem
            CompletableFuture.allOf(getSkuInfoTask,getSkuSaleAttrValues).get();

            String s = JSON.toJSONString(cartItem);
            cartOps.put(skuId.toString(),s);

            return cartItem;



        }
        else {
            //购物车有此商品,修改数量
            CartItem cartItem = JSON.parseObject(res, CartItem.class);
            cartItem.setCount(cartItem.getCount()+num);

            cartOps.put(skuId.toString(),JSON.toJSONString(cartItem));
            return cartItem;
        }

    }

    @Override
    public CartItem getCartItem(Long skuId) {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        String str= (String) cartOps.get(skuId.toString());
        CartItem cartItem = JSON.parseObject(str, CartItem.class);
        return cartItem;
    }

    @Override
    public Cart getCart() throws ExecutionException, InterruptedException {

        Cart cart = new Cart();
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        if (userInfoTo.getUserId()!=null)
        {
            //1.登陆
            String cartKey =CART_PZREFIX+userInfoTo.getUserId();
            //2.如果临时购物车的数据还没有进行合并[合并购物车]
            String tempCartKey=CART_PZREFIX+userInfoTo.getUserkey();
            List<CartItem> tempCartItems = getCartItems(tempCartKey);
            if (tempCartItems!=null)
            {
                //临时购物车有数据，需要合并
                for (CartItem item : tempCartItems) {
                    addToCart(item.getSkuId(),item.getCount());
                }
                //清除临时购物车的数据
                clearCart(tempCartKey);
            }

            //3.获取登陆后的购物车[包含合并过来的临时购物车的数据，和登陆后的购物车数据]
            List<CartItem> cartItems = getCartItems(cartKey);
            cart.setItems(cartItems);

        }
        else {
            //2.没登陆
            String cartKey =CART_PZREFIX+userInfoTo.getUserkey();
            //获取临时购物车的所有购物项
            List<CartItem> cartItems = getCartItems(cartKey);
            cart.setItems(cartItems);

        }

        return cart;
    }


    /**
     * 获取到我们要操作的购物车
     *
     * */
    private BoundHashOperations<String, Object, Object> getCartOps() {
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        String cartKey="";
        if (userInfoTo.getUserId()!=null)
        {
            cartKey=CART_PZREFIX+userInfoTo.getUserId();
        }else {
            cartKey=CART_PZREFIX+userInfoTo.getUserkey();
        }

        BoundHashOperations<String, Object, Object> operations = redisTemplate.boundHashOps(cartKey);

        return operations;
    }

    private List<CartItem> getCartItems(String cartkey)
    {
        BoundHashOperations<String, Object, Object> hashOps = redisTemplate.boundHashOps(cartkey);
        List<Object> values = hashOps.values();
        if (values!=null&&values.size()>0)
        {
            List<CartItem> collect = values.stream().map((obj) -> {
                String str=(String)obj;
                CartItem cartItem = JSON.parseObject(str, CartItem.class);
                return cartItem;
            }).collect(Collectors.toList());
            return collect;
        }

        return null;
    }

    @Override
    public void clearCart(String cartkey)
    {
       redisTemplate.delete(cartkey);


    }

    @Override
    public void checkItem(Long skuId, Integer checked) {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        CartItem cartItem = getCartItem(skuId);
        cartItem.setCheck(checked==1?true:false);
        String s = JSON.toJSONString(cartItem);
        cartOps.put(skuId.toString(),s);

    }

    @Override
    public void checkItemCount(Long skuId, Integer num) {
        CartItem cartItem = getCartItem(skuId);
        cartItem.setCount(num);

        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        cartOps.put(skuId.toString(),JSON.toJSONString(cartItem));

    }

    @Override
    public void deleteItem(Long skuId) {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        cartOps.delete(skuId.toString());

    }

    @Override
    public List<CartItem> getUserCartItems() {
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        if (userInfoTo.getUserId()==null) {
            getCartItems(userInfoTo.getUserkey());
            return null;
        }else
        {
            String cartKey=CART_PZREFIX+userInfoTo.getUserId();
            List<CartItem> cartItems = getCartItems(cartKey);

            //获取所有被选中的购物项
            List<CartItem> collect = cartItems.stream().filter(item -> item.getCheck()).map(item -> {
                //TODO 更新为最新价格-->远程调用product服务
                R newPrice = productFeignService.getPrice(item.getSkuId());
                String data = (String) newPrice.get("data");
                item.setPrice(new BigDecimal(data));
                return item;
            }).collect(Collectors.toList());

            return  collect;


        }

    }
}
