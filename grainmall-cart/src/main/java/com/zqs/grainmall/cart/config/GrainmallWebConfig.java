package com.zqs.grainmall.cart.config;

import com.zqs.grainmall.cart.interceptor.CartInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @program: grain-mall
 * @description: 对拦截器的一些配置
 * @author: Mr.Zhang
 * @create: 2020-11-18 23:10
 **/
@Configuration
public class GrainmallWebConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //添加我们自定义的拦截器并且添加只要在cart服务下，所有请求都拦截
        registry.addInterceptor(new CartInterceptor()).addPathPatterns("/**");
    }
}
